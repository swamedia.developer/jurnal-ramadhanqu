final version = '1.0.2';

const kDbHelper = 'helper';
const kDbProfile = 'userProfile';
const kDbTkShaum = 'tkshaum';
const kDbTkMurajaah = 'tkmurajaah';
const kDbTkShalat = 'tkshalat';
const kDbSdShaum = 'sdshaum';
const kDbSdMurajaah = 'sdmurajaah';
const kDbSdShalat = 'sdshalat';

List listShalat = [
  'Maghrib',
  'Isya',
  'Tarawih',
  'Shubuh',
  'Dzuhur',
  'Ashar',
];

List listQuranTitle = [
  'NAMA SURAT',
  'AYAT',
  'CHECK',
];

List listDay = [
  1,
  2,
  3,
  4,
  5,
  6,
  7,
  8,
  9,
  10,
  11,
  12,
  13,
  14,
  15,
  16,
  17,
  18,
  19,
  20,
  21,
  22,
  23,
  24,
  25,
  26,
  27,
  28,
  29,
  30,
];
