import 'package:flutter/material.dart';

const kProductTitleStyleLarge =
    TextStyle(fontSize: 18, fontWeight: FontWeight.bold);

const kTextField = InputDecoration(
  hintText: '',
  contentPadding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
  border: OutlineInputBorder(
    borderRadius: BorderRadius.all(Radius.circular(20.0)),
  ),
  enabledBorder: OutlineInputBorder(
    borderSide: BorderSide(
      color: Colors.black,
      width: 2.0,
    ),
    borderRadius: BorderRadius.all(Radius.circular(20.0)),
  ),
);

const kBottomSheet = RoundedRectangleBorder(
  borderRadius: BorderRadius.vertical(
    top: Radius.circular(25.0),
  ),
);

const kDropDown = ShapeDecoration(
  shape: RoundedRectangleBorder(
    side: BorderSide(
      width: 1.0,
      style: BorderStyle.solid,
      color: Colors.blueGrey,
    ),
    borderRadius: BorderRadius.all(Radius.circular(3.0)),
  ),
);

const kCircleContainer = CircleBorder();

ButtonStyle kButtonStyle = ButtonStyle(
  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
    RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(15.0),
    ),
  ),
);

var kMessageTextField = InputDecoration(
  hintText: '',
  contentPadding: EdgeInsets.symmetric(
    vertical: 10.0,
    horizontal: 20.0,
  ),
  border: OutlineInputBorder(
    borderRadius: BorderRadius.all(Radius.circular(10.0)),
  ),
  enabledBorder: OutlineInputBorder(
    borderSide: BorderSide(
      color: Colors.black,
      width: 2.0,
    ),
    borderRadius: BorderRadius.all(Radius.circular(10.0)),
  ),
);
