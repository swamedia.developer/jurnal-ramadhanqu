import 'package:flutter/material.dart';

Color primaryColor = Color(0xffa1aa29);
Color secondaryColor = Color(0xfffe8101);
Color tertiaryColor = Color(0xff42a224);
Color redColor = Colors.red;
Color blueColor = Colors.blue;
Color greenColor = Colors.green;
Color yellowCollor = Colors.yellow;
Color pinkColor = Colors.pink;
Color purpleColor = Colors.purple;
Color orangeColor = Colors.orange;
Color indigoColor = Colors.indigo;
Color brownColor = Color(0xff52331f);
Color sdOrangeColor = Color(0xffff5100);
Color sdYellowColor = Color(0xffe4bb17);
Color sdGreenColor = Color(0xff5bc33c);

int primaryColorCode = 0xffa1aa29;
int secondaryColorCode = 0xfffe8101;
int tertiaryColorCode = 0xff42a224;
int brownColorCode = 0xff52331f;
int sdOrangeColorCode = 0xffff5100;
int sdYellowColorCode = 0xffe4bb17;
int sdGreenColorCode = 0xff5bc33c;

Map<int, Color> color = {
  50: Color.fromRGBO(51, 153, 255, .1),
  100: Color.fromRGBO(51, 153, 255, .2),
  200: Color.fromRGBO(51, 153, 255, .3),
  300: Color.fromRGBO(51, 153, 255, .4),
  400: Color.fromRGBO(51, 153, 255, .5),
  500: Color.fromRGBO(51, 153, 255, .6),
  600: Color.fromRGBO(51, 153, 255, .7),
  700: Color.fromRGBO(51, 153, 255, .8),
  800: Color.fromRGBO(51, 153, 255, .9),
  900: Color.fromRGBO(51, 153, 255, 1),
};

Color headerColor(code) {
  switch (code) {
    case 'tkshaum':
      return primaryColor;
    case 'tkmurajaah':
      return secondaryColor;
    case 'tkshalat':
      return tertiaryColor;
    case 'sdshaum':
      return sdOrangeColor;
    case 'sdmurajaah':
      return sdYellowColor;
    case 'sdshalat':
      return sdGreenColor;
    default:
      return primaryColor;
  }
}
