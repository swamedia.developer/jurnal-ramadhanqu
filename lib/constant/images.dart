const kBackground = 'assets/images/img-background.jpg';
const kLogoTitle = 'assets/images/logo-title.png';
const kLogoTKSD = 'assets/images/logo-tksd.png';
const kLogoTaujih = 'assets/images/logo-taujih.png';
const kImgUstad = 'assets/images/img-ustad.png';
const kImgMenu = 'assets/images/img-menu.png';
const kLogoTK = 'assets/images/logo-tk.png';
const kLogoSD = 'assets/images/logo-sd.png';
const kLogoOTP = 'assets/images/logo-otp.png';
const kLoading = 'assets/images/loading.gif';
const kImgShaum = 'assets/images/img-shaum.png';
const kImgQuran = 'assets/images/img-quran.png';
const kImgTilawah = 'assets/images/img-tilawah.png';
const kImgShalat = 'assets/images/img-shalat.png';
const kImgSplash1 = 'assets/images/img-splash-1.jpg';
const kImgSplash2 = 'assets/images/img-splash-2.jpg';
const kImgSplash3 = 'assets/images/img-splash-3.jpg';
const kImgSplash4 = 'assets/images/img-splash-4.jpg';
const kImgEmpty = 'assets/images/img-empty.png';

List splashImgList = [
  kImgSplash1,
  kImgSplash2,
  kImgSplash3,
  kImgSplash4,
];