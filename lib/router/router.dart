import 'package:flutter/material.dart';
import 'package:swamediajurnalramadhanqu/pages/home.dart';
import 'package:swamediajurnalramadhanqu/pages/login.dart';
import 'package:swamediajurnalramadhanqu/pages/main_menu.dart';
import 'package:swamediajurnalramadhanqu/pages/profile.dart';
import 'package:swamediajurnalramadhanqu/pages/welcome_splash.dart';
import 'package:swamediajurnalramadhanqu/pages/taujih.dart';
import 'package:swamediajurnalramadhanqu/router/constants.dart';

const abdcds = false;

class Router {
  static Route<dynamic>? generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case homePage:
        return MaterialPageRoute(
          settings: settings,
          builder: (_) => HomePage(),
        );
      case profilePage:
        return MaterialPageRoute(
          settings: settings,
          builder: (_) => ProfilePage(),
        );
      case taujihPage:
        return MaterialPageRoute(
          settings: settings,
          builder: (_) => TaujihPage(),
        );
      case mainMenuPage:
        return MaterialPageRoute(
          settings: settings,
          builder: (_) => MainMenuPage(),
        );
      case loginPage:
        return MaterialPageRoute(
          settings: settings,
          builder: (_) => LoginPage(),
        );
      case splashPage:
        return MaterialPageRoute(
          settings: settings,
          builder: (_) => MainMenuSplashPage(),
        );
      default:
        return null;
    }
  }
}
