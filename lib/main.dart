import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:swamediajurnalramadhanqu/constant/colors.dart';
import 'package:swamediajurnalramadhanqu/provider/helper.dart';
import 'package:swamediajurnalramadhanqu/provider/login.dart';
import 'package:swamediajurnalramadhanqu/provider/profile.dart';
import 'package:swamediajurnalramadhanqu/router/router.dart' as RouterGen;
import 'pages/home.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  Provider.debugCheckInvalidValueType = null;
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider<HelperProvider>(
          create: (_) => HelperProvider(),
        ),
        Provider<ProfileProvider>(
          create: (_) => ProfileProvider(),
        ),
        Provider<LoginProvider>(
          create: (_) => LoginProvider(),
        ),
      ],
      child: MaterialApp(
        title: 'Jurnal Ramadhan Kidz',
        theme: ThemeData(
          primarySwatch: MaterialColor(primaryColorCode, color),
        ),
        home: HomePage(),
        debugShowCheckedModeBanner: false,
        onGenerateRoute: RouterGen.Router.generateRoute,
      ),
    );
  }
}
