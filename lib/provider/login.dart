import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:swamediajurnalramadhanqu/helper/dialogs.dart';
import 'package:swamediajurnalramadhanqu/pages/otp.dart';
import 'package:swamediajurnalramadhanqu/router/constants.dart';

class LoginProvider extends ChangeNotifier {
  FirebaseAuth auth = FirebaseAuth.instance;
  User? firebaseUser;
  String? actualCode;
  bool isOtpLoading = false;
  GlobalKey<ScaffoldState> otpScaffoldKey = GlobalKey<ScaffoldState>();

  Future<User> isAlreadyAuthenticated() async {
    firebaseUser = auth.currentUser;
    return firebaseUser!;
  }

  Future<void> verifyUser(
    BuildContext context,
    String phoneNumber,
  ) async {
    await auth.verifyPhoneNumber(
      phoneNumber: phoneNumber,
      verificationCompleted: (PhoneAuthCredential credential) async {
        await auth.signInWithCredential(credential).then((UserCredential user) {
          onAuthenticationSuccessful(context, user);
        }).catchError((error) {
          Dialogs.failDialog(context, error, 'OK');
        });
      },
      verificationFailed: (FirebaseAuthException e) {
        Dialogs.failDialog(context, e.message!, 'OKE');
      },
      codeSent: (String? verificationId, int? resendToken) async {
        actualCode = verificationId!;
        await Navigator.of(context).push(
          MaterialPageRoute(
            builder: (_) => OtpPage(phoneNumber: phoneNumber),
          ),
        );
      },
      codeAutoRetrievalTimeout: (String verificationId) {
        actualCode = verificationId;
      },
    );
  }

  Future<void> validateOtpAndLogin(BuildContext context, String smsCode) async {
    // isOtpLoading = true;
    final AuthCredential _authCredential = PhoneAuthProvider.credential(
      verificationId: actualCode!,
      smsCode: smsCode,
    );

    await auth.signInWithCredential(_authCredential).catchError((error) {
      // isOtpLoading = false;
      Dialogs.failDialog(context, error, 'OK');
    }).then((UserCredential? result) {
      if (result != null && result.user != null) {
        onAuthenticationSuccessful(context, result);
      }
    });
  }

  Future<void> onAuthenticationSuccessful(
    BuildContext context,
    UserCredential result,
  ) async {
    firebaseUser = result.user;
    Navigator.pushNamedAndRemoveUntil(
      context,
      mainMenuPage,
      (route) => false,
    );
  }

  Future<void> signOut(BuildContext context) async {
    await auth.signOut();
    await Navigator.pushNamedAndRemoveUntil(
      context,
      taujihPage,
      (route) => false,
    );
    firebaseUser = null;
  }
}
