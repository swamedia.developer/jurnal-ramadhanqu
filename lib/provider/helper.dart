import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:launch_review/launch_review.dart';
import 'package:swamediajurnalramadhanqu/constant/db_const.dart';
import 'package:swamediajurnalramadhanqu/helper/dialogs.dart';
import 'package:swamediajurnalramadhanqu/helper/snack_bar.dart';

class HelperProvider extends ChangeNotifier {
  FirebaseFirestore fireStore = FirebaseFirestore.instance;
  CollectionReference? collection;
  dynamic listVersion, listOther, listSurat;
  String? updateFrom;
  List? verList;

  Future<void> getHelperDoc(BuildContext context) async {
    collection = fireStore.collection(kDbHelper);
    try {
      await collection!.doc('appVersion').get().then((value) {
        listVersion = value.data();
      });
      await collection!.doc('other').get().then((value) {
        listOther = value.data();
      });
      await collection!.doc('surat').get().then((value) {
        listSurat = value.data();
      });
    } catch (e) {
      showSnackBar(context, e.toString());
    }
  }

  void checkVersion(BuildContext context) {
    updateFrom = listVersion['updateFrom'];
    verList = [version, updateFrom];
    verList!.sort((a, b) => a.compareTo(b));
    int newIndex = verList!.indexWhere((f) => f == updateFrom);
    int currentIndex = verList!.indexWhere((f) => f == version);
    if (updateFrom == version || currentIndex < newIndex) {
      updateApps(context);
    }
  }

  void updateApps(BuildContext context) {
    Dialogs.updateDialog(
      context,
      listVersion['updateMessage'],
      'OK',
      listVersion['dismissable'],
      () {
        LaunchReview.launch(
          writeReview: false,
          androidAppId: listVersion['playstoreID'],
        );
        // Navigator.of(context).pop();
      },
    );
  }
}
