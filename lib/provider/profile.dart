import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:swamediajurnalramadhanqu/constant/db_const.dart';
import 'package:swamediajurnalramadhanqu/helper/snack_bar.dart';
import 'package:swamediajurnalramadhanqu/provider/login.dart';

class ProfileProvider extends ChangeNotifier {
  FirebaseFirestore fireStore = FirebaseFirestore.instance;
  CollectionReference? collection;
  String? phoneNumber;
  Map<String, dynamic>? userProfile = {};
  List listAnak = [];

  Future<void> checkUser(BuildContext context) async {
    final prov = Provider.of<LoginProvider>(context, listen: false);
    phoneNumber = prov.firebaseUser!.phoneNumber;
    collection = fireStore.collection(kDbProfile);
    bool isUserExist = false;
    List listUser = [];
    try {
      await collection!.get().then((value) {
        var query = value.docs;
        for (int i = 0; i < query.length; i++) {
          listUser.add(query[i].id);
          if (phoneNumber == listUser[i]) {
            isUserExist = true;
          }
        }
        if (!isUserExist) {
          createUser(context);
        }
      });
      await getUserProfile(context);
    } catch (e) {
      showSnackBar(context, e.toString());
    }
  }

  Future<void> createUser(BuildContext context) async {
    final prov = Provider.of<LoginProvider>(context, listen: false);
    final id = prov.firebaseUser!.uid;
    await collection!.doc(phoneNumber).set({
      'ID': id,
      'No. HP': phoneNumber,
      'Nama': '',
      'Kelas': '',
      'Level': '',
      'Guru': false,
    });
  }

  Future<void> getUserProfile(BuildContext context) async {
    final prov = Provider.of<LoginProvider>(context, listen: false);
    final phoneNumber = prov.firebaseUser!.phoneNumber;
    collection = fireStore.collection(kDbProfile);
    try {
      await collection!.doc(phoneNumber).get().then((value) {
        userProfile = value.data();
      });
    } catch (e) {
      showSnackBar(context, e.toString());
    }
  }

  Future<void> getDataAnak(BuildContext context, String level) async {
    collection = fireStore.collection(kDbProfile);
    List _listAnak = [];
    try {
      await collection!.get().then((value) {
        var query = value.docs;
        for (int i = 0; i < query.length; i++) {
          final data = query[i].data();
          if (data!['Level'] == level) {
            _listAnak.add(data);
          }
        }
      });
      listAnak = _listAnak;
    } catch (e) {
      showSnackBar(context, e.toString());
    }
  }
}
