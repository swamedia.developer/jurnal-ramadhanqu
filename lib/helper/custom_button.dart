import 'package:flutter/material.dart';
import 'package:swamediajurnalramadhanqu/constant/styles.dart';

class CustomButton extends StatelessWidget {
  final Color? color;
  final Color? textColor;
  final Color? borderColor;
  final String? text;
  final double? radius;
  final double? letterSpacing;
  final VoidCallback? onPressed;

  const CustomButton({
    Key? key,
    this.color,
    this.textColor,
    this.borderColor,
    this.text,
    this.radius,
    this.letterSpacing,
    this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: kButtonStyle.copyWith(
        backgroundColor: MaterialStateProperty.all<Color>(color!),
      ),
      child: Container(
        padding: EdgeInsets.all(10.0),
        child: FittedBox(
          child: Text(
            text!,
            style: TextStyle(
              color: textColor,
              fontWeight: FontWeight.bold,
              letterSpacing: letterSpacing,
            ),
          ),
        ),
      ),
      onPressed: onPressed,
    );
  }
}
