import 'package:flutter/material.dart';
import 'package:swamediajurnalramadhanqu/constant/colors.dart';
import 'package:swamediajurnalramadhanqu/constant/images.dart';

Widget kLoadingWidget(context) {
  return Center(
    child: Image.asset(
      kLoading,
      height: 70.0,
      width: 70.0,
      fit: BoxFit.cover,
    ),
  );
}

Widget kLoadAnakWidget(context) {
  return Center(
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Image.asset(
          kLoading,
          height: 70.0,
          width: 70.0,
          fit: BoxFit.cover,
        ),
        Text(
          'Memperoleh data anak...',
          style: TextStyle(
            color: brownColor,
            fontWeight: FontWeight.bold,
          ),
        ),
      ],
    ),
  );
}
