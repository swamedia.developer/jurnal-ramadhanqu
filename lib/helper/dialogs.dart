import 'package:flutter/material.dart';
import 'package:swamediajurnalramadhanqu/constant/colors.dart';
import 'package:swamediajurnalramadhanqu/constant/images.dart';

class Dialogs {
  Dialogs();

  static Future<void> failDialog(
    BuildContext context,
    String title,
    String textButton,
  ) async {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        AlertDialog alert = AlertDialog(
          title: Text('Gagal'),
          content: Text(
            title,
            style: TextStyle(
              height: 1.5,
            ),
          ),
          actions: <Widget>[
            MaterialButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text(
                textButton,
                style: TextStyle(
                  color: primaryColor,
                ),
              ),
            ),
          ],
        );
        return alert;
      },
    );
  }

  static Future<void> customDialog(
    BuildContext context,
    String title,
    String content,
    String noButtonText,
    String yesButtonText,
    VoidCallback noButton,
    VoidCallback yesButton,
  ) async {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        AlertDialog alert = AlertDialog(
          title: Text(title),
          content: Text(
            content,
            style: TextStyle(
              height: 1.5,
            ),
          ),
          actions: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width * 0.25,
              child: ElevatedButton(
                onPressed: noButton,
                style: ElevatedButton.styleFrom(
                  primary: redColor,
                ),
                child: Text(
                  noButtonText,
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width * 0.25,
              child: ElevatedButton(
                onPressed: yesButton,
                style: ElevatedButton.styleFrom(
                  primary: primaryColor,
                ),
                child: Text(
                  yesButtonText,
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ],
        );
        return alert;
      },
    );
  }

  static Future<void> updateDialog(
    BuildContext context,
    String title,
    String textButton,
    bool isDismissable,
    VoidCallback onPressed,
  ) async {
    return showDialog(
      context: context,
      barrierDismissible: isDismissable,
      builder: (BuildContext context) {
        return WillPopScope(
          onWillPop: () async => isDismissable,
          child: AlertDialog(
            content: Container(
              width: MediaQuery.of(context).size.width * 0.5,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    title,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      height: 1.5,
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.only(
                      top: 20.0,
                    ),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: primaryColor,
                      ),
                      child: Text(
                        textButton,
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                      onPressed: onPressed,
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  static Future<void> showLoadingDialog(
    BuildContext context,
  ) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return WillPopScope(
          onWillPop: () async => true,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(15),
                child: Image.asset(
                  kLoading,
                  width: 80.0,
                  height: 80.0,
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
