import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:swamediajurnalramadhanqu/constant/colors.dart';
import 'package:swamediajurnalramadhanqu/constant/db_const.dart';
import 'package:swamediajurnalramadhanqu/helper/dialogs.dart';
import 'package:swamediajurnalramadhanqu/helper/loading.dart';
import 'package:swamediajurnalramadhanqu/helper/snack_bar.dart';
import 'package:swamediajurnalramadhanqu/provider/login.dart';
import 'package:swamediajurnalramadhanqu/widgets/header.dart';
import 'package:table_sticky_headers/table_sticky_headers.dart';

class ShalatPage extends StatefulWidget {
  final String? level, title;

  const ShalatPage({
    Key? key,
    this.level,
    this.title,
  }) : super(key: key);

  @override
  State<ShalatPage> createState() => _ShalatPageState();
}

class _ShalatPageState extends State<ShalatPage> {
  FirebaseFirestore fireStore = FirebaseFirestore.instance;
  CollectionReference? collection;
  ScrollControllers scrollController = ScrollControllers();
  double _scrollOffsetX = 0.0;
  double _scrollOffsetY = 0.0;
  String? phoneNumber;
  List listUser = [];
  dynamic checkShalat;
  bool isShalatExist = false;
  bool isLoading = false;

  @override
  void initState() {
    super.initState();
    final table = (widget.level! + widget.title!).toLowerCase();
    collection = fireStore.collection(table);
    checkDataShalat();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future checkDataShalat() async {
    isLoading = true;
    final prov = Provider.of<LoginProvider>(context, listen: false);
    phoneNumber = prov.firebaseUser!.phoneNumber;
    try {
      await collection!.get().then((value) {
        var query = value.docs;
        for (int i = 0; i < query.length; i++) {
          listUser.add(query[i].id);
          if (phoneNumber == listUser[i]) {
            isShalatExist = true;
          }
        }
        if (!isShalatExist) {
          createShalat();
        }
      });
    } catch (e) {
      showSnackBar(context, e.toString());
    }
    await getDataShalat();
    setState(() {
      isLoading = false;
    });
  }

  Future createShalat() async {
    await collection!.doc(phoneNumber).set({
      '1': {
        listShalat[0]: false,
        listShalat[1]: false,
        listShalat[2]: false,
        listShalat[3]: false,
        listShalat[4]: false,
        listShalat[5]: false,
      },
      '2': {
        listShalat[0]: false,
        listShalat[1]: false,
        listShalat[2]: false,
        listShalat[3]: false,
        listShalat[4]: false,
        listShalat[5]: false,
      },
      '3': {
        listShalat[0]: false,
        listShalat[1]: false,
        listShalat[2]: false,
        listShalat[3]: false,
        listShalat[4]: false,
        listShalat[5]: false,
      },
      '4': {
        listShalat[0]: false,
        listShalat[1]: false,
        listShalat[2]: false,
        listShalat[3]: false,
        listShalat[4]: false,
        listShalat[5]: false,
      },
      '5': {
        listShalat[0]: false,
        listShalat[1]: false,
        listShalat[2]: false,
        listShalat[3]: false,
        listShalat[4]: false,
        listShalat[5]: false,
      },
      '6': {
        listShalat[0]: false,
        listShalat[1]: false,
        listShalat[2]: false,
        listShalat[3]: false,
        listShalat[4]: false,
        listShalat[5]: false,
      },
      '7': {
        listShalat[0]: false,
        listShalat[1]: false,
        listShalat[2]: false,
        listShalat[3]: false,
        listShalat[4]: false,
        listShalat[5]: false,
      },
      '8': {
        listShalat[0]: false,
        listShalat[1]: false,
        listShalat[2]: false,
        listShalat[3]: false,
        listShalat[4]: false,
        listShalat[5]: false,
      },
      '9': {
        listShalat[0]: false,
        listShalat[1]: false,
        listShalat[2]: false,
        listShalat[3]: false,
        listShalat[4]: false,
        listShalat[5]: false,
      },
      '10': {
        listShalat[0]: false,
        listShalat[1]: false,
        listShalat[2]: false,
        listShalat[3]: false,
        listShalat[4]: false,
        listShalat[5]: false,
      },
      '11': {
        listShalat[0]: false,
        listShalat[1]: false,
        listShalat[2]: false,
        listShalat[3]: false,
        listShalat[4]: false,
        listShalat[5]: false,
      },
      '12': {
        listShalat[0]: false,
        listShalat[1]: false,
        listShalat[2]: false,
        listShalat[3]: false,
        listShalat[4]: false,
        listShalat[5]: false,
      },
      '13': {
        listShalat[0]: false,
        listShalat[1]: false,
        listShalat[2]: false,
        listShalat[3]: false,
        listShalat[4]: false,
        listShalat[5]: false,
      },
      '14': {
        listShalat[0]: false,
        listShalat[1]: false,
        listShalat[2]: false,
        listShalat[3]: false,
        listShalat[4]: false,
        listShalat[5]: false,
      },
      '15': {
        listShalat[0]: false,
        listShalat[1]: false,
        listShalat[2]: false,
        listShalat[3]: false,
        listShalat[4]: false,
        listShalat[5]: false,
      },
      '16': {
        listShalat[0]: false,
        listShalat[1]: false,
        listShalat[2]: false,
        listShalat[3]: false,
        listShalat[4]: false,
        listShalat[5]: false,
      },
      '17': {
        listShalat[0]: false,
        listShalat[1]: false,
        listShalat[2]: false,
        listShalat[3]: false,
        listShalat[4]: false,
        listShalat[5]: false,
      },
      '18': {
        listShalat[0]: false,
        listShalat[1]: false,
        listShalat[2]: false,
        listShalat[3]: false,
        listShalat[4]: false,
        listShalat[5]: false,
      },
      '19': {
        listShalat[0]: false,
        listShalat[1]: false,
        listShalat[2]: false,
        listShalat[3]: false,
        listShalat[4]: false,
        listShalat[5]: false,
      },
      '20': {
        listShalat[0]: false,
        listShalat[1]: false,
        listShalat[2]: false,
        listShalat[3]: false,
        listShalat[4]: false,
        listShalat[5]: false,
      },
      '21': {
        listShalat[0]: false,
        listShalat[1]: false,
        listShalat[2]: false,
        listShalat[3]: false,
        listShalat[4]: false,
        listShalat[5]: false,
      },
      '22': {
        listShalat[0]: false,
        listShalat[1]: false,
        listShalat[2]: false,
        listShalat[3]: false,
        listShalat[4]: false,
        listShalat[5]: false,
      },
      '23': {
        listShalat[0]: false,
        listShalat[1]: false,
        listShalat[2]: false,
        listShalat[3]: false,
        listShalat[4]: false,
        listShalat[5]: false,
      },
      '24': {
        listShalat[0]: false,
        listShalat[1]: false,
        listShalat[2]: false,
        listShalat[3]: false,
        listShalat[4]: false,
        listShalat[5]: false,
      },
      '25': {
        listShalat[0]: false,
        listShalat[1]: false,
        listShalat[2]: false,
        listShalat[3]: false,
        listShalat[4]: false,
        listShalat[5]: false,
      },
      '26': {
        listShalat[0]: false,
        listShalat[1]: false,
        listShalat[2]: false,
        listShalat[3]: false,
        listShalat[4]: false,
        listShalat[5]: false,
      },
      '27': {
        listShalat[0]: false,
        listShalat[1]: false,
        listShalat[2]: false,
        listShalat[3]: false,
        listShalat[4]: false,
        listShalat[5]: false,
      },
      '28': {
        listShalat[0]: false,
        listShalat[1]: false,
        listShalat[2]: false,
        listShalat[3]: false,
        listShalat[4]: false,
        listShalat[5]: false,
      },
      '29': {
        listShalat[0]: false,
        listShalat[1]: false,
        listShalat[2]: false,
        listShalat[3]: false,
        listShalat[4]: false,
        listShalat[5]: false,
      },
      '30': {
        listShalat[0]: false,
        listShalat[1]: false,
        listShalat[2]: false,
        listShalat[3]: false,
        listShalat[4]: false,
        listShalat[5]: false,
      },
    });
  }

  Future getDataShalat() async {
    final prov = Provider.of<LoginProvider>(context, listen: false);
    phoneNumber = prov.firebaseUser!.phoneNumber;
    try {
      await collection!.doc(phoneNumber).get().then((value) {
        checkShalat = value.data();
      });
    } catch (e) {
      showSnackBar(context, e.toString());
    }
  }

  Future onChecked(String shalat, int number, bool checked) async {
    String day = number.toString();
    final tempData = checkShalat[day];
    tempData[shalat] = checked;
    final dataToSend = tempData;
    Dialogs.showLoadingDialog(context);
    try {
      await collection!.doc(phoneNumber).update({
        day: dataToSend,
      });
    } catch (e) {
      showSnackBar(context, e.toString());
    }
    await getDataShalat();
    Navigator.of(context).pop();
    setState(() {});
  }

  Widget buildCells = Column(
    children: List.generate(
      30,
      (index) {
        return Align(
          alignment: Alignment.topLeft,
          child: Container(
            padding: EdgeInsets.symmetric(
              vertical: 5.0,
              horizontal: 8.0,
            ),
            width: 35.0,
            decoration: BoxDecoration(
              color: primaryColor,
              borderRadius: BorderRadius.circular(20.0),
            ),
            child: Text(
              (index + 1).toString(),
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.white,
              ),
            ),
          ),
        );
      },
    ),
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          MenuHeader(
            level: widget.level,
            title: widget.title,
          ),
          Container(
            padding: EdgeInsets.symmetric(vertical: 30.0),
            child: FittedBox(
              child: Text(
                'Hari ini aku shalat berjamaah',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 22.0,
                ),
              ),
            ),
          ),
          isLoading
              ? kLoadingWidget(context)
              : Expanded(
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 20.0),
                    child: StickyHeadersTable(
                      columnsLength: 6,
                      rowsLength: 30,
                      cellDimensions: CellDimensions.variableColumnWidth(
                        columnWidths: [
                          100.0,
                          100.0,
                          100.0,
                          100.0,
                          100.0,
                          100.0,
                        ],
                        contentCellHeight: 80.0,
                        stickyLegendHeight: 50.0,
                        stickyLegendWidth: 50.0,
                      ),
                      scrollControllers: scrollController,
                      initialScrollOffsetX: _scrollOffsetX,
                      initialScrollOffsetY: _scrollOffsetY,
                      onEndScrolling: (scrollOffsetX, scrollOffsetY) {
                        _scrollOffsetX = scrollOffsetX;
                        _scrollOffsetY = scrollOffsetY;
                      },
                      columnsTitleBuilder: (i) {
                        return Text(listShalat[i]);
                      },
                      rowsTitleBuilder: (i) {
                        return Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                            padding: EdgeInsets.symmetric(
                              vertical: 5.0,
                              horizontal: 8.0,
                            ),
                            width: 35.0,
                            decoration: BoxDecoration(
                              color: primaryColor,
                              borderRadius: BorderRadius.circular(20.0),
                            ),
                            child: Text(
                              (i + 1).toString(),
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Colors.white,
                              ),
                            ),
                          ),
                        );
                      },
                      contentCellBuilder: (i, j) {
                        final isChecked = checkShalat != null
                            ? checkShalat[(j + 1).toString()][listShalat[i]]
                            : false;

                        return GestureDetector(
                          onTap: () {
                            onChecked(listShalat[i], j + 1, !isChecked);
                          },
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15.0),
                              border: Border.all(color: Colors.black),
                            ),
                            margin: EdgeInsets.only(
                              left: 5.0,
                              right: 5.0,
                              bottom: 15.0,
                            ),
                            width: 88.0,
                            height: 70.0,
                            child: isChecked ? Icon(Icons.check) : null,
                          ),
                        );
                      },
                    ),
                  ),
                ),
        ],
      ),
    );
  }
}
