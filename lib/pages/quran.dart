import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:swamediajurnalramadhanqu/constant/colors.dart';
import 'package:swamediajurnalramadhanqu/constant/db_const.dart';
import 'package:swamediajurnalramadhanqu/helper/dialogs.dart';
import 'package:swamediajurnalramadhanqu/helper/loading.dart';
import 'package:swamediajurnalramadhanqu/helper/snack_bar.dart';
import 'package:swamediajurnalramadhanqu/provider/login.dart';
import 'package:swamediajurnalramadhanqu/widgets/ayat.dart';
import 'package:swamediajurnalramadhanqu/widgets/header.dart';
import 'package:swamediajurnalramadhanqu/widgets/list_surat.dart';
import 'package:table_sticky_headers/table_sticky_headers.dart';

class QuranPage extends StatefulWidget {
  final String? level, title;
  final int? day;

  const QuranPage({
    Key? key,
    this.level,
    this.title,
    this.day,
  }) : super(key: key);

  @override
  State<QuranPage> createState() => _QuranPageState();
}

class _QuranPageState extends State<QuranPage> {
  FirebaseFirestore fireStore = FirebaseFirestore.instance;
  CollectionReference? collection;
  ScrollControllers scrollController = ScrollControllers();
  double _scrollOffsetX = 0.0;
  double _scrollOffsetY = 0.0;
  String? phoneNumber;
  List listUser = [];
  dynamic checkMurajaah;
  bool isMurajaahExist = false;
  bool isLoading = false;

  @override
  void initState() {
    super.initState();
    final table = (widget.level! + widget.title!).toLowerCase();
    collection = fireStore.collection(table);
    checkDataMurajaah();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future checkDataMurajaah() async {
    isLoading = true;
    final prov = Provider.of<LoginProvider>(context, listen: false);
    phoneNumber = prov.firebaseUser!.phoneNumber;
    try {
      await collection!.get().then((value) {
        var query = value.docs;
        for (int i = 0; i < query.length; i++) {
          listUser.add(query[i].id);
          if (phoneNumber == listUser[i]) {
            isMurajaahExist = true;
          }
        }
        if (!isMurajaahExist) {
          createMurajaah();
        }
      });
    } catch (e) {
      showSnackBar(context, e.toString());
    }
    await getDataMurajaah();
    setState(() {
      isLoading = false;
    });
  }

  Future createMurajaah() async {
    await collection!.doc(phoneNumber).set({
      '1': {},
      '2': {},
      '3': {},
      '4': {},
      '5': {},
      '6': {},
      '7': {},
      '8': {},
      '9': {},
      '10': {},
      '11': {},
      '12': {},
      '13': {},
      '14': {},
      '15': {},
      '16': {},
      '17': {},
      '18': {},
      '19': {},
      '20': {},
      '21': {},
      '22': {},
      '23': {},
      '24': {},
      '25': {},
      '26': {},
      '27': {},
      '28': {},
      '29': {},
      '30': {},
    });
  }

  Future getDataMurajaah() async {
    final prov = Provider.of<LoginProvider>(context, listen: false);
    phoneNumber = prov.firebaseUser!.phoneNumber;
    try {
      await collection!.doc(phoneNumber).get().then((value) {
        checkMurajaah = value.data();
      });
    } catch (e) {
      showSnackBar(context, e.toString());
    }
  }

  Future pickSurat(int day, number, String ayat, bool checked) async {
    final result = await showModalBottomSheet(
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(30.0),
          topRight: Radius.circular(30.0),
        ),
      ),
      builder: (context) {
        return ListSurat();
      },
    );
    if (result != null) {
      final _day = day.toString();
      final _number = (number + 1).toString();
      final tempData = checkMurajaah[_day];
      tempData[_number] = {
        'Surat': result,
        'Ayat': ayat,
        'Check': checked,
      };
      final dataToSend = tempData;
      Dialogs.showLoadingDialog(context);
      try {
        await collection!.doc(phoneNumber).update({
          _day: dataToSend,
        });
      } catch (e) {
        showSnackBar(context, e.toString());
      }
      await getDataMurajaah();
      Navigator.of(context).pop();
      setState(() {});
    }
  }

  Future pickAyat(int day, number, String surat, ayat, bool checked) async {
    final result = await showModalBottomSheet(
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(30.0),
          topRight: Radius.circular(30.0),
        ),
      ),
      isScrollControlled: true,
      builder: (context) {
        return Ayat(ayat: ayat);
      },
    );
    if (result != null) {
      final _day = day.toString();
      final _number = (number + 1).toString();
      final tempData = checkMurajaah[_day];
      tempData[_number] = {
        'Surat': surat,
        'Ayat': result,
        'Check': checked,
      };
      final dataToSend = tempData;
      Dialogs.showLoadingDialog(context);
      try {
        await collection!.doc(phoneNumber).update({
          _day: dataToSend,
        });
      } catch (e) {
        showSnackBar(context, e.toString());
      }
      await getDataMurajaah();
      Navigator.of(context).pop();
      setState(() {});
    }
  }

  Future onChecked(int day, number, String surat, ayat, bool checked) async {
    final _day = day.toString();
    final _number = (number + 1).toString();
    final tempData = checkMurajaah[_day];
    tempData[_number] = {
      'Surat': surat,
      'Ayat': ayat,
      'Check': checked,
    };
    final dataToSend = tempData;
    Dialogs.showLoadingDialog(context);
    try {
      await collection!.doc(phoneNumber).update({
        _day: dataToSend,
      });
    } catch (e) {
      showSnackBar(context, e.toString());
    }
    await getDataMurajaah();
    Navigator.of(context).pop();
    setState(() {});
  }

  Widget buildCells(int i, index) {
    final _day = (widget.day).toString();
    final _number = (index + 1).toString();
    var surat;
    var ayat = '';
    bool _checked = false;
    if (checkMurajaah != null) {
      if (checkMurajaah[_day] != null) {
        if (checkMurajaah[_day][_number] != null) {
          surat = checkMurajaah[_day][_number];
        }
      }
    }
    if (surat != null) {
      if (surat['Ayat'] != null) {
        ayat = surat['Ayat'];
      }
    }
    if (surat != null) {
      if (surat['Check'] != null) {
        _checked = surat['Check'];
      }
    }

    switch (i) {
      case 0:
        return GestureDetector(
          child: Container(
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.symmetric(
              vertical: 10.0,
              horizontal: 20.0,
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15.0),
              border: Border.all(color: Colors.black),
            ),
            child: Stack(
              children: [
                Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                    padding: EdgeInsets.only(right: 20.0),
                    child: Text(
                      surat != null ? surat['Surat'] : '',
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.centerRight,
                  child: Icon(Icons.arrow_drop_down),
                ),
              ],
            ),
            height: 58.0,
          ),
          onTap: () {
            pickSurat(
              widget.day!,
              index,
              ayat,
              _checked,
            );
          },
        );

      case 1:
        return GestureDetector(
          child: Container(
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.symmetric(
              vertical: 10.0,
              horizontal: 20.0,
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15.0),
              border: Border.all(color: Colors.black),
            ),
            child: Align(
              alignment: Alignment.center,
              child: Text(
                ayat,
                overflow: TextOverflow.ellipsis,
              ),
            ),
            height: 58.0,
          ),
          onTap: () {
            if (surat != null) {
              pickAyat(
                widget.day!,
                index,
                surat['Surat'],
                ayat,
                _checked,
              );
            } else {
              showSnackBar(context, 'Pilih surat dahulu');
            }
          },
        );
      case 2:
        return GestureDetector(
          child: Container(
            padding: EdgeInsets.symmetric(
              vertical: 10.0,
              horizontal: 20.0,
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15.0),
              border: Border.all(color: Colors.black),
            ),
            height: 58.0,
            width: 80.0,
            child: _checked ? Icon(Icons.check) : null,
          ),
          onTap: () {
            if (surat == null) {
              showSnackBar(context, 'Pilih surat dahulu');
            } else if (ayat == '') {
              showSnackBar(context, 'Pilih ayat dahulu');
            } else {
              onChecked(
                widget.day!,
                index,
                surat['Surat'],
                ayat,
                !_checked,
              );
            }
          },
        );
      default:
        return Container();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          MenuHeader(
            level: widget.level,
            title: widget.title,
          ),
          Container(
            padding: EdgeInsets.all(30.0),
            child: FittedBox(
              child: Text(
                'Hari ini aku ' + (widget.title!).toLowerCase(),
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 22.0,
                ),
              ),
            ),
          ),
          isLoading
              ? kLoadingWidget(context)
              : Expanded(
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 20.0),
                    child: StickyHeadersTable(
                      columnsLength: 3,
                      rowsLength: 30,
                      cellDimensions: CellDimensions.variableColumnWidth(
                        columnWidths: [
                          250.0,
                          100.0,
                          100.0,
                        ],
                        contentCellHeight: 80.0,
                        stickyLegendHeight: 50.0,
                        stickyLegendWidth: 50.0,
                      ),
                      scrollControllers: scrollController,
                      initialScrollOffsetX: _scrollOffsetX,
                      initialScrollOffsetY: _scrollOffsetY,
                      onEndScrolling: (scrollOffsetX, scrollOffsetY) {
                        _scrollOffsetX = scrollOffsetX;
                        _scrollOffsetY = scrollOffsetY;
                      },
                      columnsTitleBuilder: (i) {
                        return Text(
                          listQuranTitle[i],
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        );
                      },
                      rowsTitleBuilder: (i) {
                        return Container(
                          padding: EdgeInsets.only(top: 5.0),
                          child: Align(
                            alignment: Alignment.topLeft,
                            child: Container(
                              padding: EdgeInsets.symmetric(
                                vertical: 5.0,
                                horizontal: 8.0,
                              ),
                              width: 35.0,
                              decoration: BoxDecoration(
                                color: primaryColor,
                                borderRadius: BorderRadius.circular(20.0),
                              ),
                              child: Text(
                                (i + 1).toString(),
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ),
                        );
                      },
                      contentCellBuilder: (i, j) {
                        return Container(
                          margin: EdgeInsets.only(
                            left: 5.0,
                            right: 5.0,
                            bottom: 15.0,
                          ),
                          child: buildCells(i, j),
                        );
                      },
                    ),
                  ),
                ),
        ],
      ),
    );
  }
}
