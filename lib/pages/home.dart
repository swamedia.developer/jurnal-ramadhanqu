import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:swamediajurnalramadhanqu/constant/colors.dart';
import 'package:swamediajurnalramadhanqu/constant/db_const.dart';
import 'package:swamediajurnalramadhanqu/constant/images.dart';
import 'package:swamediajurnalramadhanqu/helper/custom_button.dart';
import 'package:swamediajurnalramadhanqu/helper/loading.dart';
import 'package:swamediajurnalramadhanqu/helper/snack_bar.dart';
import 'package:swamediajurnalramadhanqu/provider/helper.dart';
import 'package:swamediajurnalramadhanqu/router/constants.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  bool isLoading = false;

  @override
  void initState() {
    super.initState();
    init();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void init() async {
    final prov = Provider.of<HelperProvider>(context, listen: false);
    isLoading = true;
    try {
      await prov.getHelperDoc(context);
    } catch (e) {
      showSnackBar(context, e.toString());
    }
    checkVersion(prov);
  }

  void checkVersion(HelperProvider prov) {
    try {
      prov.checkVersion(context);
    } catch (e) {
      showSnackBar(context, e.toString());
    }
    setState(() {
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget spacer = SizedBox(height: 30.0);
    var screenWidth = MediaQuery.of(context).size.width;
    var screenHeight = MediaQuery.of(context).size.height;

    return Scaffold(
      body: Container(
        padding: EdgeInsets.only(
          left: 50.0,
          right: 50.0,
          top: 80.0,
          bottom: 30.0,
        ),
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(kBackground),
            fit: BoxFit.cover,
          ),
        ),
        child: isLoading
            ? kLoadingWidget(context)
            : Stack(
                children: [
                  Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.only(top: screenHeight * 0.1),
                    child: Column(
                      children: [
                        FittedBox(
                          child: Text(
                            'Selamat Datang',
                            style: TextStyle(
                              fontSize: 22.0,
                              fontWeight: FontWeight.bold,
                              color: brownColor,
                            ),
                          ),
                        ),
                        FittedBox(
                          child: Text(
                            'di',
                            style: TextStyle(
                              fontSize: 22.0,
                              fontWeight: FontWeight.bold,
                              color: brownColor,
                            ),
                          ),
                        ),
                        spacer,
                        Image.asset(
                          kLogoTitle,
                          width: screenWidth * 0.6,
                        ),
                        spacer,
                        Image.asset(
                          kLogoTKSD,
                          width: screenWidth * 0.4,
                        ),
                      ],
                    ),
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                      width: screenWidth * 0.35,
                      padding: EdgeInsets.only(bottom: 150.0),
                      child: CustomButton(
                        color: secondaryColor,
                        letterSpacing: 5.0,
                        radius: 15.0,
                        text: 'NEXT',
                        textColor: Colors.white,
                        onPressed: () {
                          Navigator.pushNamed(context, splashPage);
                        },
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.bottomRight,
                    child: Text(
                      'ver $version',
                      style: TextStyle(
                        color: brownColor,
                      ),
                    ),
                  ),
                ],
              ),
      ),
    );
  }
}
