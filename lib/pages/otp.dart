import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:provider/provider.dart';
import 'package:swamediajurnalramadhanqu/constant/colors.dart';
import 'package:swamediajurnalramadhanqu/constant/images.dart';
import 'package:swamediajurnalramadhanqu/helper/dialogs.dart';
import 'package:swamediajurnalramadhanqu/helper/otp/otp_field_style.dart';
import 'package:swamediajurnalramadhanqu/helper/otp/otp_text_field.dart';
import 'package:swamediajurnalramadhanqu/provider/login.dart';
import 'package:swamediajurnalramadhanqu/widgets/loader_hud.dart';

class OtpPage extends StatefulWidget {
  final String? phoneNumber;

  const OtpPage({
    Key? key,
    this.phoneNumber,
  }) : super(key: key);

  @override
  _OtpPageState createState() => _OtpPageState();
}

class _OtpPageState extends State<OtpPage> {
  String otpText = '';
  OtpFieldController otpController = OtpFieldController();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<void> validateOTP(LoginProvider prov) async {
    Dialogs.showLoadingDialog(context);
    try {
      await prov.validateOtpAndLogin(context, otpText);
    } catch (e) {
      Navigator.of(context).pop();
      Dialogs.failDialog(
        context,
        'Kode OTP yang kamu masukkan salah',
        'OK',
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    var screenWidth = MediaQuery.of(context).size.width;
    var consumer = Consumer<LoginProvider>(
      builder: (_, prov, __) {
        return Observer(
          builder: (_) => LoaderHUD(
            inAsyncCall: prov.isOtpLoading,
            child: Scaffold(
              backgroundColor: Colors.white,
              key: prov.otpScaffoldKey,
              appBar: AppBar(
                backgroundColor: Colors.white,
                leading: IconButton(
                  icon: Container(
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(20)),
                    ),
                    child: Icon(
                      Icons.arrow_back_ios,
                      size: 16,
                      color: Colors.black,
                    ),
                  ),
                  onPressed: () => Navigator.of(context).pop(),
                ),
                elevation: 0,
              ),
              body: SingleChildScrollView(
                child: Container(
                  padding: EdgeInsets.all(20.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Container(
                        child: Image.asset(kLogoOTP),
                      ),
                      SizedBox(height: 30.0),
                      Container(
                        child: Text(
                          'Konfirmasi OTP',
                          style: TextStyle(
                            fontSize: 26,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      SizedBox(height: 10.0),
                      FittedBox(
                        child: Row(
                          children: [
                            Text(
                              'Masukan kode OTP yang telah dikirimkan ke' + ' ',
                              style: TextStyle(
                                color: Colors.grey,
                              ),
                            ),
                            Text(
                              widget.phoneNumber!,
                              style: TextStyle(
                                color: primaryColor,
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: 30.0),
                      OTPTextField(
                        controller: otpController,
                        length: 6,
                        borderWidth: 5.0,
                        width: MediaQuery.of(context).size.width,
                        textFieldAlignment: MainAxisAlignment.spaceAround,
                        fieldWidth: 50,
                        style: TextStyle(
                          fontSize: 22.0,
                        ),
                        fieldStyle: FieldStyle.box,
                        outlineBorderRadius: 8,
                        otpFieldStyle: OtpFieldStyle(
                          focusBorderColor: primaryColor,
                        ),
                        onChanged: (pin) {
                          otpText = pin;
                        },
                        onCompleted: (pin) {
                          otpText = pin;
                        },
                      ),
                    ],
                  ),
                ),
              ),
              bottomNavigationBar: Container(
                margin: EdgeInsets.all(15.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    // FittedBox(
                    //   child: Row(
                    //     mainAxisAlignment: MainAxisAlignment.center,
                    //     children: [
                    //       Container(
                    //         child: Text(S().resendOTP + ' '),
                    //       ),
                    //       Container(
                    //         child: GestureDetector(
                    //           onTap: () {
                    //             //
                    //           },
                    //           child: Text(
                    //             S().kirimUlang,
                    //             style: TextStyle(
                    //               color: kColorPrimary,
                    //             ),
                    //           ),
                    //         ),
                    //       ),
                    //     ],
                    //   ),
                    // ),
                    // SizedBox(height: 30.0),
                    Container(
                      width: screenWidth,
                      child: ElevatedButton(
                        onPressed: otpText.length != 6
                            ? null
                            : () {
                                validateOTP(prov);
                              },
                        child: Text(
                          'VERIFIKASI',
                          style: TextStyle(color: Colors.white),
                        ),
                        style: ElevatedButton.styleFrom(
                          padding: EdgeInsets.all(15.0),
                          primary: primaryColor,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
    return consumer;
  }
}
