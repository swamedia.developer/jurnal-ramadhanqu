import 'package:country_code_picker/country_code.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:swamediajurnalramadhanqu/constant/colors.dart';
import 'package:swamediajurnalramadhanqu/constant/images.dart';
import 'package:swamediajurnalramadhanqu/constant/styles.dart';
import 'package:swamediajurnalramadhanqu/helper/custom_button.dart';
import 'package:swamediajurnalramadhanqu/helper/dialogs.dart';
import 'package:swamediajurnalramadhanqu/helper/snack_bar.dart';
import 'package:swamediajurnalramadhanqu/provider/login.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController phoneController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  String? phoneNumber, _phone;
  CountryCode countryCode = CountryCode(
    code: 'ID',
    dialCode: '+62',
    name: 'Indonesia',
  );

  @override
  void initState() {
    super.initState();
    _phone = '';
    // countryCode = CountryCode(
    //   code: 'ID',
    //   dialCode: '+62',
    //   name: 'Indonesia',
    // );
    phoneController.addListener(
      () {
        if (phoneController.text != _phone && phoneController.text != '') {
          _phone = phoneController.text;
          onPhoneNumberChange(
            _phone!,
            '${countryCode.dialCode}$_phone',
            countryCode.code!,
          );
        }
      },
    );
  }

  void onPhoneNumberChange(
    String number,
    String internationalizedPhoneNumber,
    String isoCode,
  ) {
    if (internationalizedPhoneNumber.isNotEmpty) {
      phoneNumber = internationalizedPhoneNumber;
    } else {
      phoneNumber = null;
    }
  }

  onLogin(LoginProvider prov) async {
    Dialogs.showLoadingDialog(context);
    if (phoneNumber != null) {
      if (phoneNumber!.substring(3).startsWith('0')) {
        phoneNumber = '+62' + phoneNumber!.substring(4);
      }
      try {
        await prov.verifyUser(context, phoneNumber!);
        // Navigator.of(context).pop();
      } catch (e) {
        // Navigator.of(context).pop();
        showSnackBar(context, e.toString());
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    var screenWidth = MediaQuery.of(context).size.width;
    var screenHeight = MediaQuery.of(context).size.height;

    return Consumer<LoginProvider>(
      builder: (context, prov, a) {
        return Scaffold(
          body: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage(kBackground),
                fit: BoxFit.cover,
              ),
            ),
            child: ListView(
              padding: EdgeInsets.symmetric(
                horizontal: 30.0,
                vertical: screenHeight * 0.1,
              ),
              children: [
                Container(
                  padding: EdgeInsets.symmetric(vertical: 20.0),
                  child: Image.asset(kLogoTitle),
                ),
                Card(
                  elevation: 10.0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  shadowColor: Colors.black,
                  child: Column(
                    children: [
                      Container(
                        alignment: Alignment.centerLeft,
                        padding: EdgeInsets.only(
                          top: 30.0,
                          left: 30.0,
                          right: 30.0,
                        ),
                        child: Text(
                          'Selamat Datang!',
                          style: TextStyle(
                            fontSize: 15.0,
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.symmetric(
                          vertical: 20.0,
                          horizontal: 30.0,
                        ),
                        child: TextFormField(
                          controller: phoneController,
                          decoration: kTextField.copyWith(
                            hintText: 'Masukan no HP kamu',
                            prefixIcon: Icon(Icons.phone_android),
                          ),
                          cursorColor: primaryColor,
                          inputFormatters: [
                            FilteringTextInputFormatter.allow(RegExp("[0-9]"))
                          ],
                          keyboardType: TextInputType.number,
                          validator: (val) =>
                              val!.isEmpty ? 'No HP tidak boleh kosong' : null,
                        ),
                      ),
                      Container(
                        width: screenWidth * 0.35,
                        child: CustomButton(
                          color: primaryColor,
                          radius: 10.0,
                          text: 'LOGIN',
                          textColor: Colors.white,
                          onPressed: () {
                            onLogin(prov);
                          },
                        ),
                      ),
                      SizedBox(
                        height: 50.0,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
