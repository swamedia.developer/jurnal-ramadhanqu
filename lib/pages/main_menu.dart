import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:swamediajurnalramadhanqu/constant/colors.dart';
import 'package:swamediajurnalramadhanqu/constant/db_const.dart';
import 'package:swamediajurnalramadhanqu/constant/images.dart';
import 'package:swamediajurnalramadhanqu/helper/custom_button.dart';
import 'package:swamediajurnalramadhanqu/helper/dialogs.dart';
import 'package:swamediajurnalramadhanqu/helper/loading.dart';
import 'package:swamediajurnalramadhanqu/helper/snack_bar.dart';
import 'package:swamediajurnalramadhanqu/pages/sub_menu.dart';
import 'package:swamediajurnalramadhanqu/provider/helper.dart';
import 'package:swamediajurnalramadhanqu/provider/login.dart';
import 'package:swamediajurnalramadhanqu/provider/profile.dart';
import 'package:swamediajurnalramadhanqu/router/constants.dart';
import 'package:swamediajurnalramadhanqu/widgets/buttonMenu.dart';
import 'package:swamediajurnalramadhanqu/widgets/mode_guru.dart';

class MainMenuPage extends StatefulWidget {
  const MainMenuPage({Key? key}) : super(key: key);

  @override
  State<MainMenuPage> createState() => _MainMenuPageState();
}

class _MainMenuPageState extends State<MainMenuPage> {
  FirebaseFirestore fireStore = FirebaseFirestore.instance;
  CollectionReference? collection;
  bool isLoading = false;

  @override
  void initState() {
    super.initState();
    getUserProfile();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future getUserProfile() async {
    isLoading = true;
    final prov = Provider.of<ProfileProvider>(context, listen: false);
    try {
      await prov.checkUser(context);
    } catch (e) {
      showSnackBar(context, e.toString());
    }
    setState(() {
      isLoading = false;
    });
  }

  void editProfile() async {
    final result = await Navigator.pushNamed(context, profilePage);
    if (result != null) {
      await getUserProfile();
      showSnackBar(context, 'Profil berhasil diubah');
    }
  }

  void onLogout() {
    final prov = Provider.of<LoginProvider>(context, listen: false);
    Dialogs.customDialog(
      context,
      'Peringatan',
      'Yakin mau keluar?',
      'Tidak',
      'Ya',
      () => Navigator.of(context).pop(),
      () async {
        Dialogs.showLoadingDialog(context);
        await prov.signOut(context);
      },
    );
  }

  void confirmModeGuru() async {
    final result = await showModalBottomSheet(
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(30.0),
          topRight: Radius.circular(30.0),
        ),
      ),
      isScrollControlled: true,
      builder: (context) {
        return ModeGuru();
      },
    );
    if (result != null) {
      final prov = Provider.of<HelperProvider>(context, listen: false);
      final code = prov.listOther['guruCode'];
      if (result == code) {
        modeGuru(true);
      } else {
        showSnackBar(context, 'Code salah');
      }
      if (result == 'exit') {
        modeGuru(false);
      }
    }
  }

  Future<void> modeGuru(bool enter) async {
    final prov = Provider.of<LoginProvider>(context, listen: false);
    final profileProv = Provider.of<ProfileProvider>(context, listen: false);
    final phoneNumber = prov.firebaseUser!.phoneNumber;
    collection = fireStore.collection(kDbProfile);
    Dialogs.showLoadingDialog(context);
    try {
      await collection!.doc(phoneNumber).update({
        'Guru': enter,
      });
      await profileProv.getUserProfile(context);
      Navigator.of(context).pop();
      setState(() {});
      showSnackBar(
        context,
        enter ? 'Berhasil masuk ke mode guru' : 'Anda keluar dari Mode Guru',
      );
    } catch (e) {
      Navigator.of(context).pop();
      showSnackBar(context, e.toString());
    }
  }

  // Future help() async {
  //   collection = fireStore.collection(kDbProfile);
  //   List listUser = [];
  //   try {
  //     await collection!.get().then((value) {
  //       var query = value.docs;
  //       for (int i = 0; i < query.length; i++) {
  //         listUser.add(query[i].id);
  //         collection!.doc(listUser[i]).update({
  //           'Guru': false,
  //         });
  //       }
  //     });
  //   } catch (e) {
  //     showSnackBar(context, e.toString());
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    final prov = Provider.of<ProfileProvider>(context, listen: false);
    final loginProv = Provider.of<LoginProvider>(context, listen: false);
    final userName = prov.userProfile!['Nama'];
    final phoneNumber = loginProv.firebaseUser!.phoneNumber;
    final name = userName != null && userName != '' ? userName : phoneNumber;
    final isGuru = prov.userProfile!['Guru'];
    var screenHeight = MediaQuery.of(context).size.height;

    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(kBackground),
            fit: BoxFit.cover,
          ),
        ),
        child: isLoading
            ? kLoadingWidget(context)
            : ListView(
                padding: EdgeInsets.symmetric(
                  horizontal: 50.0,
                  vertical: screenHeight * 0.2,
                ),
                children: [
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 30.0),
                    child: Image.asset(kImgMenu),
                  ),
                  SizedBox(height: 20.0),
                  GestureDetector(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          child: Text(
                            'Hai, ',
                            style: TextStyle(
                              fontSize: 16.0,
                            ),
                          ),
                        ),
                        Container(
                          child: Text(
                            name.contains(' ')
                                ? name.substring(0, name.indexOf(' '))
                                : name,
                            style: TextStyle(
                              fontSize: 16.0,
                              color: primaryColor,
                            ),
                          ),
                        ),
                        SizedBox(width: 10.0),
                        Icon(
                          CupertinoIcons.chevron_down,
                          size: 20.0,
                        ),
                      ],
                    ),
                    onTap: () async {
                      editProfile();
                    },
                  ),
                  isGuru
                      ? Container(
                          padding: EdgeInsets.only(top: 20.0),
                          child: Text(
                            'Anda berada di Mode Guru',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 16.0,
                              color: secondaryColor,
                            ),
                          ),
                        )
                      : Container(),
                  SizedBox(height: 30.0),
                  FittedBox(
                    child: Row(
                      children: [
                        ButtonMenu(
                          image: kLogoTK,
                          title: 'TK AlQuran',
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (_) => SubMenu(level: 'TK'),
                              ),
                            );
                          },
                        ),
                        SizedBox(width: 70.0),
                        ButtonMenu(
                          image: kLogoSD,
                          title: 'SD AlQuran',
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (_) => SubMenu(level: 'SD'),
                              ),
                            );
                          },
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 20.0),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 70.0),
                    child: CustomButton(
                      color: primaryColor,
                      radius: 10.0,
                      text: 'LOGOUT',
                      textColor: Colors.white,
                      onPressed: () {
                        onLogout();
                      },
                    ),
                  ),
                  GestureDetector(
                    child: Container(
                      margin: EdgeInsets.only(top: 50.0),
                      color: redColor.withOpacity(0.0),
                      height: 50.0,
                      width: 50.0,
                    ),
                    onDoubleTap: () {
                      confirmModeGuru();
                    },
                  ),
                  // Container(
                  //   padding: EdgeInsets.symmetric(horizontal: 70.0),
                  //   child: CustomButton(
                  //     color: primaryColor,
                  //     radius: 10.0,
                  //     text: 'TEST',
                  //     textColor: Colors.white,
                  //     onPressed: () {
                  //       help();
                  //     },
                  //   ),
                  // ),
                ],
              ),
      ),
    );
  }
}
