import 'dart:math';

import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:swamediajurnalramadhanqu/constant/images.dart';
import 'package:swamediajurnalramadhanqu/constant/sounds.dart';
import 'package:swamediajurnalramadhanqu/router/constants.dart';

class MainMenuSplashPage extends StatefulWidget {
  const MainMenuSplashPage({Key? key}) : super(key: key);

  @override
  State<MainMenuSplashPage> createState() => _MainMenuSplashPageState();
}

class _MainMenuSplashPageState extends State<MainMenuSplashPage> {
  AudioCache audioCache = AudioCache();
  AudioPlayer audioPlayer = AudioPlayer();
  int index = 0;

  @override
  void initState() {
    super.initState();
    pickImage();
    startSound();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void pickImage() {
    var rng = Random();
    index = rng.nextInt(4);
  }

  void startSound() async {
    audioPlayer = await audioCache.play(kSoundWelcome);
    audioPlayer.onPlayerCompletion.listen((event) {
      Navigator.pushNamed(context, taujihPage);
    });
  }

  Future<bool> onWillPop() async {
    audioPlayer.stop();
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: WillPopScope(
        onWillPop: onWillPop,
        child: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(
                splashImgList[index],
              ),
              fit: BoxFit.fitHeight,
            ),
          ),
        ),
      ),
    );
  }
}
