import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:swamediajurnalramadhanqu/helper/dialogs.dart';
import 'package:swamediajurnalramadhanqu/helper/loading.dart';
import 'package:swamediajurnalramadhanqu/helper/snack_bar.dart';
import 'package:swamediajurnalramadhanqu/provider/login.dart';
import 'package:swamediajurnalramadhanqu/widgets/check_box_shaum.dart';
import 'package:swamediajurnalramadhanqu/widgets/header.dart';

class ShaumPage extends StatefulWidget {
  final String? level, title;

  const ShaumPage({
    Key? key,
    this.level,
    this.title,
  }) : super(key: key);

  @override
  State<ShaumPage> createState() => _ShaumPageState();
}

class _ShaumPageState extends State<ShaumPage> {
  FirebaseFirestore fireStore = FirebaseFirestore.instance;
  CollectionReference? collection;
  String? phoneNumber;
  List listUser = [];
  dynamic checkShaum;
  bool isShaumExist = false;
  bool isLoading = false;

  @override
  void initState() {
    super.initState();
    final table = (widget.level! + widget.title!).toLowerCase();
    collection = fireStore.collection(table);
    checkDataShaum();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future checkDataShaum() async {
    isLoading = true;
    final prov = Provider.of<LoginProvider>(context, listen: false);
    phoneNumber = prov.firebaseUser!.phoneNumber;
    try {
      await collection!.get().then((value) {
        var query = value.docs;
        for (int i = 0; i < query.length; i++) {
          listUser.add(query[i].id);
          if (phoneNumber == listUser[i]) {
            isShaumExist = true;
          }
        }
        if (!isShaumExist) {
          createShaum();
        }
      });
    } catch (e) {
      showSnackBar(context, e.toString());
    }
    await getDataShaum();
    setState(() {
      isLoading = false;
    });
  }

  Future createShaum() async {
    await collection!.doc(phoneNumber).set({
      '1': false,
      '2': false,
      '3': false,
      '4': false,
      '5': false,
      '6': false,
      '7': false,
      '8': false,
      '9': false,
      '10': false,
      '11': false,
      '12': false,
      '13': false,
      '14': false,
      '15': false,
      '16': false,
      '17': false,
      '18': false,
      '19': false,
      '20': false,
      '21': false,
      '22': false,
      '23': false,
      '24': false,
      '25': false,
      '26': false,
      '27': false,
      '28': false,
      '29': false,
      '30': false,
    });
  }

  Future getDataShaum() async {
    final prov = Provider.of<LoginProvider>(context, listen: false);
    phoneNumber = prov.firebaseUser!.phoneNumber;
    try {
      await collection!.doc(phoneNumber).get().then((value) {
        checkShaum = value.data();
      });
    } catch (e) {
      showSnackBar(context, e.toString());
    }
  }

  Future onChecked(int number, bool checked) async {
    Dialogs.showLoadingDialog(context);
    try {
      await collection!.doc(phoneNumber).update({
        number.toString(): checked,
      });
    } catch (e) {
      showSnackBar(context, e.toString());
    }
    await getDataShaum();
    Navigator.of(context).pop();
    setState(() {});
  }

  Row dayRow(String title, int row) {
    final isFirstRow = row < 2;
    final column1 = isFirstRow ? row : row + (2 * (row - 1));
    final column2 = isFirstRow ? row + 1 : (column1 + 1);
    final column3 = isFirstRow ? row + 2 : (column1 + 2);
    final ckColum1 = checkShaum != null ? checkShaum[column1.toString()] : false;
    final ckColum2 = checkShaum != null ? checkShaum[column2.toString()] : false;
    final ckColum3 = checkShaum != null ? checkShaum[column3.toString()] : false;

    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        CheckBoxShaum(
          number: column1,
          isChecked: ckColum1,
          onTap: () => onChecked(column1, !ckColum1),
        ),
        SizedBox(width: 20.0),
        CheckBoxShaum(
          number: column2,
          isChecked: ckColum2,
          onTap: () => onChecked(column2, !ckColum2),
        ),
        SizedBox(width: 20.0),
        CheckBoxShaum(
          number: column3,
          isChecked: ckColum3,
          onTap: () => onChecked(column3, !ckColum3),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          MenuHeader(
            level: widget.level,
            title: widget.title,
          ),
          Container(
            padding: EdgeInsets.only(top: 30.0),
            child: FittedBox(
              child: Text(
                'Checklist Shaumku',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 22.0,
                ),
              ),
            ),
          ),
          isLoading
              ? kLoadingWidget(context)
              : Expanded(
                  child: ListView.builder(
                    padding: EdgeInsets.all(30.0),
                    itemCount: 10,
                    itemBuilder: (context, index) {
                      return Column(
                        children: [
                          FittedBox(
                            child: dayRow(widget.title!, index + 1),
                          ),
                          SizedBox(height: 10.0),
                        ],
                      );
                    },
                  ),
                ),
        ],
      ),
    );
  }
}
