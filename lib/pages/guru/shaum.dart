import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:swamediajurnalramadhanqu/constant/colors.dart';
import 'package:swamediajurnalramadhanqu/constant/styles.dart';
import 'package:swamediajurnalramadhanqu/helper/custom_button.dart';
import 'package:swamediajurnalramadhanqu/helper/dialogs.dart';
import 'package:swamediajurnalramadhanqu/helper/snack_bar.dart';
import 'package:swamediajurnalramadhanqu/provider/profile.dart';
import 'package:swamediajurnalramadhanqu/widgets/appbar.dart';
import 'package:swamediajurnalramadhanqu/widgets/check_box_shaum.dart';
import 'package:swamediajurnalramadhanqu/widgets/empty.dart';
import 'package:swamediajurnalramadhanqu/widgets/list_anak.dart';
import 'package:swamediajurnalramadhanqu/widgets/visibility.dart';

class GuruShaumPage extends StatefulWidget {
  final String? level, title;

  const GuruShaumPage({
    Key? key,
    this.level,
    this.title,
  }) : super(key: key);

  @override
  State<GuruShaumPage> createState() => _GuruShaumPageState();
}

class _GuruShaumPageState extends State<GuruShaumPage> {
  FirebaseFirestore fireStore = FirebaseFirestore.instance;
  CollectionReference? collection;
  TextEditingController nameController = TextEditingController();
  Map<String, dynamic> dataAnak = {};
  dynamic dataShaum;
  String namaAnak = '';

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void pickName() async {
    final prov = Provider.of<ProfileProvider>(context, listen: false);
    final listAnak = prov.listAnak;
    listAnak.sort(
        (a, b) => a['Nama'].toUpperCase().compareTo(b['Nama'].toUpperCase()));
    final result = await showModalBottomSheet(
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(30.0),
          topRight: Radius.circular(30.0),
        ),
      ),
      builder: (context) {
        return ListAnak(listAnak: listAnak);
      },
    );
    if (result != null) {
      setState(() {
        dataAnak = result;
        nameController.text = dataAnak['Nama']!;
      });
    }
  }

  Future<void> onSearch(Map<String, dynamic> dataAnak) async {
    if (dataAnak.isEmpty) {
      showSnackBar(context, 'Pilih nama anak terlebih dahulu');
    } else {
      final phoneNumber = dataAnak['No. HP'];
      final table = (widget.level! + widget.title!).toLowerCase();
      collection = fireStore.collection(table);
      Dialogs.showLoadingDialog(context);
      try {
        await collection!.doc(phoneNumber).get().then((value) {
          dataShaum = value.data() != null ? value.data() : null;
        });
        setState(() {
          namaAnak = dataAnak.isNotEmpty ? dataAnak['Nama'] : '';
        });
        Navigator.of(context).pop();
      } catch (e) {
        Navigator.of(context).pop();
        showSnackBar(context, e.toString());
      }
    }
  }

  Row dayRow(String title, int row) {
    final isFirstRow = row < 2;
    final column1 = isFirstRow ? row : row + (2 * (row - 1));
    final column2 = isFirstRow ? row + 1 : (column1 + 1);
    final column3 = isFirstRow ? row + 2 : (column1 + 2);
    final ckColum1 = dataShaum != null ? dataShaum[column1.toString()] : false;
    final ckColum2 = dataShaum != null ? dataShaum[column2.toString()] : false;
    final ckColum3 = dataShaum != null ? dataShaum[column3.toString()] : false;

    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        CheckBoxShaum(
          number: column1,
          isChecked: ckColum1,
          isGuru: true,
          onTap: () => null,
        ),
        SizedBox(width: 20.0),
        CheckBoxShaum(
          number: column2,
          isChecked: ckColum2,
          isGuru: true,
          onTap: () => null,
        ),
        SizedBox(width: 20.0),
        CheckBoxShaum(
          number: column3,
          isChecked: ckColum3,
          isGuru: true,
          onTap: () => null,
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: customAppBar(
        context,
        widget.title! + ' ' + widget.level!,
      ),
      body: Column(
        children: [
          SizedBox(height: 20.0),
          Container(
            padding: EdgeInsets.symmetric(
              horizontal: 30.0,
              vertical: 10.0,
            ),
            child: Row(
              children: [
                Expanded(
                  child: TextField(
                    controller: nameController,
                    decoration: kTextField.copyWith(
                      hintText: 'Pilih nama anak',
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(15.0)),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Colors.black,
                        ),
                        borderRadius: BorderRadius.all(Radius.circular(15.0)),
                      ),
                    ),
                    readOnly: true,
                    onTap: pickName,
                  ),
                ),
                SizedBox(width: 20.0),
                CustomButton(
                  color: primaryColor,
                  textColor: Colors.white,
                  text: 'CARI',
                  onPressed: () => onSearch(dataAnak),
                ),
              ],
            ),
          ),
          Expanded(
            child: VisibilityWidget(
              visible:
                  dataAnak.isNotEmpty && dataShaum != null && namaAnak != '',
              replacement: EmptyWidget(),
              child: Column(
                children: [
                  Container(
                    alignment: Alignment.centerLeft,
                    padding: EdgeInsets.symmetric(
                      horizontal: 30.0,
                      vertical: 10.0,
                    ),
                    child: Row(
                      children: [
                        Text('Nama anak : '),
                        Text(
                          namaAnak,
                          style: TextStyle(
                            color: secondaryColor,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: ListView.builder(
                      padding: EdgeInsets.symmetric(
                        horizontal: 30.0,
                        vertical: 20.0,
                      ),
                      itemCount: 10,
                      itemBuilder: (context, index) {
                        return Column(
                          children: [
                            FittedBox(
                              child: dayRow(widget.title!, index + 1),
                            ),
                            SizedBox(height: 10.0),
                          ],
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
