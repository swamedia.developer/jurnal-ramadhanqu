import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:swamediajurnalramadhanqu/constant/colors.dart';
import 'package:swamediajurnalramadhanqu/constant/db_const.dart';
import 'package:swamediajurnalramadhanqu/constant/styles.dart';
import 'package:swamediajurnalramadhanqu/helper/custom_button.dart';
import 'package:swamediajurnalramadhanqu/helper/dialogs.dart';
import 'package:swamediajurnalramadhanqu/helper/snack_bar.dart';
import 'package:swamediajurnalramadhanqu/provider/profile.dart';
import 'package:swamediajurnalramadhanqu/widgets/appbar.dart';
import 'package:swamediajurnalramadhanqu/widgets/empty.dart';
import 'package:swamediajurnalramadhanqu/widgets/list_anak.dart';
import 'package:swamediajurnalramadhanqu/widgets/list_dynamic.dart';
import 'package:swamediajurnalramadhanqu/widgets/visibility.dart';
import 'package:table_sticky_headers/table_sticky_headers.dart';

class GuruQuranPage extends StatefulWidget {
  final String? level, title;

  const GuruQuranPage({
    Key? key,
    this.level,
    this.title,
  }) : super(key: key);

  @override
  State<GuruQuranPage> createState() => _GuruQuranPageState();
}

class _GuruQuranPageState extends State<GuruQuranPage> {
  FirebaseFirestore fireStore = FirebaseFirestore.instance;
  CollectionReference? collection;
  TextEditingController nameController = TextEditingController();
  TextEditingController dayController = TextEditingController();
  Map<String, dynamic> dataAnak = {};
  dynamic dataQuran;
  String namaAnak = '';
  String day = '';
  ScrollControllers scrollController = ScrollControllers();
  double _scrollOffsetX = 0.0;
  double _scrollOffsetY = 0.0;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void pickName() async {
    final prov = Provider.of<ProfileProvider>(context, listen: false);
    final listAnak = prov.listAnak;
    listAnak.sort(
        (a, b) => a['Nama'].toUpperCase().compareTo(b['Nama'].toUpperCase()));
    final result = await showModalBottomSheet(
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(30.0),
          topRight: Radius.circular(30.0),
        ),
      ),
      builder: (context) {
        return ListAnak(listAnak: listAnak);
      },
    );
    if (result != null) {
      setState(() {
        dataAnak = result;
        nameController.text = dataAnak['Nama']!;
      });
    }
  }

  void pickDay() async {
    final result = await showModalBottomSheet(
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(30.0),
          topRight: Radius.circular(30.0),
        ),
      ),
      builder: (context) {
        return ListDynamic(
          data: listDay,
          title: 'Ramadhan hari ke',
          isCenterText: true,
        );
      },
    );
    if (result != null) {
      setState(() {
        dayController.text = result;
      });
    }
  }

  Future<void> onSearch(Map<String, dynamic> dataAnak, String _day) async {
    if (dataAnak.isEmpty) {
      showSnackBar(context, 'Pilih nama anak terlebih dahulu');
    } else if (dayController.text == '') {
      showSnackBar(context, 'Pilih hari terlebih dahulu');
    } else {
      final phoneNumber = dataAnak['No. HP'];
      final table = (widget.level! + widget.title!).toLowerCase();
      collection = fireStore.collection(table);
      Dialogs.showLoadingDialog(context);
      try {
        await collection!.doc(phoneNumber).get().then((value) {
          dataQuran = value.data() != null ? value.data()![_day] : null;
        });
        setState(() {
          namaAnak = dataAnak.isNotEmpty ? dataAnak['Nama'] : '';
          day = _day;
        });
        Navigator.of(context).pop();
      } catch (e) {
        Navigator.of(context).pop();
        showSnackBar(context, e.toString());
      }
    }
  }

  Widget buildCells(int i, index) {
    final _number = (index + 1).toString();
    var surat;
    var ayat = '';
    if (dataQuran != null) {
      if (dataQuran[_number] != null) {
        surat = dataQuran[_number];
      }
    }
    if (surat != null) {
      if (surat['Ayat'] != null) {
        ayat = surat['Ayat'];
      }
    }

    switch (i) {
      case 0:
        return GestureDetector(
          child: Container(
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.symmetric(
              vertical: 10.0,
              horizontal: 20.0,
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15.0),
              border: Border.all(color: Colors.black),
            ),
            child: Stack(
              children: [
                Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                    padding: EdgeInsets.only(right: 20.0),
                    child: Text(
                      surat != null ? surat['Surat'] : '',
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                ),
              ],
            ),
            height: 58.0,
          ),
          onTap: null,
        );

      case 1:
        return GestureDetector(
          child: Container(
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.symmetric(
              vertical: 10.0,
              horizontal: 20.0,
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15.0),
              border: Border.all(color: Colors.black),
            ),
            child: Align(
              alignment: Alignment.center,
              child: Text(
                ayat,
                overflow: TextOverflow.ellipsis,
              ),
            ),
            height: 58.0,
          ),
          onTap: null,
        );
      default:
        return Container();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: customAppBar(
        context,
        widget.title! + ' ' + widget.level!,
      ),
      body: Column(
        children: [
          SizedBox(height: 20.0),
          Container(
            padding: EdgeInsets.symmetric(
              horizontal: 30.0,
              vertical: 10.0,
            ),
            child: TextField(
              controller: nameController,
              decoration: kTextField.copyWith(
                hintText: '',
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(15.0)),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Colors.black,
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(15.0)),
                ),
                prefixIcon: Container(
                  padding: EdgeInsets.only(
                    left: 15.0,
                    right: 5.0,
                  ),
                  child: Text(
                    'Nama anak : ',
                    textAlign: TextAlign.center,
                  ),
                ),
                prefixIconConstraints: BoxConstraints(
                  minWidth: 0,
                  minHeight: 0,
                ),
              ),
              readOnly: true,
              onTap: pickName,
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(
              horizontal: 30.0,
              vertical: 10.0,
            ),
            child: Row(
              children: [
                Expanded(
                  child: TextField(
                    controller: dayController,
                    decoration: kTextField.copyWith(
                      hintText: '',
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(15.0)),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Colors.black,
                        ),
                        borderRadius: BorderRadius.all(Radius.circular(15.0)),
                      ),
                      prefixIcon: Container(
                        padding: EdgeInsets.only(
                          left: 15.0,
                          right: 5.0,
                        ),
                        child: Text(
                          'Hari ke : ',
                          textAlign: TextAlign.center,
                        ),
                      ),
                      prefixIconConstraints: BoxConstraints(
                        minWidth: 0,
                        minHeight: 0,
                      ),
                    ),
                    readOnly: true,
                    onTap: pickDay,
                  ),
                ),
                SizedBox(width: 20.0),
                Container(
                  width: 100.0,
                  child: CustomButton(
                    color: primaryColor,
                    textColor: Colors.white,
                    text: 'CARI',
                    onPressed: () => onSearch(dataAnak, dayController.text),
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: VisibilityWidget(
              visible: dataAnak.isNotEmpty &&
                  dataQuran != null &&
                  namaAnak != '' &&
                  day != '',
              replacement: EmptyWidget(),
              child: Column(
                children: [
                  Container(
                    alignment: Alignment.centerLeft,
                    padding: EdgeInsets.symmetric(
                      horizontal: 30.0,
                      vertical: 10.0,
                    ),
                    child: Row(
                      children: [
                        Text('Nama anak : '),
                        Text(
                          namaAnak,
                          style: TextStyle(
                            color: secondaryColor,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    padding: EdgeInsets.symmetric(
                      horizontal: 30.0,
                      vertical: 10.0,
                    ),
                    child: Row(
                      children: [
                        Text('Hari ke : '),
                        Text(
                          day,
                          style: TextStyle(
                            color: secondaryColor,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 20.0),
                      child: StickyHeadersTable(
                        columnsLength: 2,
                        rowsLength: 30,
                        cellDimensions: CellDimensions.variableColumnWidth(
                          columnWidths: [
                            250.0,
                            100.0,
                          ],
                          contentCellHeight: 80.0,
                          stickyLegendHeight: 50.0,
                          stickyLegendWidth: 50.0,
                        ),
                        scrollControllers: scrollController,
                        initialScrollOffsetX: _scrollOffsetX,
                        initialScrollOffsetY: _scrollOffsetY,
                        onEndScrolling: (scrollOffsetX, scrollOffsetY) {
                          _scrollOffsetX = scrollOffsetX;
                          _scrollOffsetY = scrollOffsetY;
                        },
                        columnsTitleBuilder: (i) {
                          return Text(
                            listQuranTitle[i],
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          );
                        },
                        rowsTitleBuilder: (i) {
                          return Container(
                            padding: EdgeInsets.only(top: 5.0),
                            child: Align(
                              alignment: Alignment.topLeft,
                              child: Container(
                                padding: EdgeInsets.symmetric(
                                  vertical: 5.0,
                                  horizontal: 8.0,
                                ),
                                width: 35.0,
                                decoration: BoxDecoration(
                                  color: primaryColor,
                                  borderRadius: BorderRadius.circular(20.0),
                                ),
                                child: Text(
                                  (i + 1).toString(),
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            ),
                          );
                        },
                        contentCellBuilder: (i, j) {
                          return Container(
                            margin: EdgeInsets.only(
                              left: 5.0,
                              right: 5.0,
                              bottom: 15.0,
                            ),
                            child: buildCells(i, j),
                          );
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
