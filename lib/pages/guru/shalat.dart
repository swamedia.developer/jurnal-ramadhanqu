import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:swamediajurnalramadhanqu/constant/colors.dart';
import 'package:swamediajurnalramadhanqu/constant/db_const.dart';
import 'package:swamediajurnalramadhanqu/constant/styles.dart';
import 'package:swamediajurnalramadhanqu/helper/custom_button.dart';
import 'package:swamediajurnalramadhanqu/helper/dialogs.dart';
import 'package:swamediajurnalramadhanqu/helper/snack_bar.dart';
import 'package:swamediajurnalramadhanqu/provider/profile.dart';
import 'package:swamediajurnalramadhanqu/widgets/appbar.dart';
import 'package:swamediajurnalramadhanqu/widgets/empty.dart';
import 'package:swamediajurnalramadhanqu/widgets/list_anak.dart';
import 'package:swamediajurnalramadhanqu/widgets/visibility.dart';
import 'package:table_sticky_headers/table_sticky_headers.dart';

class GuruShalatPage extends StatefulWidget {
  final String? level, title;

  const GuruShalatPage({
    Key? key,
    this.level,
    this.title,
  }) : super(key: key);

  @override
  State<GuruShalatPage> createState() => _GuruShalatPageState();
}

class _GuruShalatPageState extends State<GuruShalatPage> {
  FirebaseFirestore fireStore = FirebaseFirestore.instance;
  CollectionReference? collection;
  TextEditingController nameController = TextEditingController();
  Map<String, dynamic> dataAnak = {};
  dynamic dataShalat;
  String namaAnak = '';
  ScrollControllers scrollController = ScrollControllers();
  double _scrollOffsetX = 0.0;
  double _scrollOffsetY = 0.0;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void pickName() async {
    final prov = Provider.of<ProfileProvider>(context, listen: false);
    final listAnak = prov.listAnak;
    listAnak.sort(
        (a, b) => a['Nama'].toUpperCase().compareTo(b['Nama'].toUpperCase()));
    final result = await showModalBottomSheet(
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(30.0),
          topRight: Radius.circular(30.0),
        ),
      ),
      builder: (context) {
        return ListAnak(listAnak: listAnak);
      },
    );
    if (result != null) {
      setState(() {
        dataAnak = result;
        nameController.text = dataAnak['Nama']!;
      });
    }
  }

  Future<void> onSearch(Map<String, dynamic> dataAnak) async {
    if (dataAnak.isEmpty) {
      showSnackBar(context, 'Pilih nama anak terlebih dahulu');
    } else {
      final phoneNumber = dataAnak['No. HP'];
      final table = (widget.level! + widget.title!).toLowerCase();
      collection = fireStore.collection(table);
      Dialogs.showLoadingDialog(context);
      try {
        await collection!.doc(phoneNumber).get().then((value) {
          dataShalat = value.data() != null ? value.data() : null;
        });
        setState(() {
          namaAnak = dataAnak.isNotEmpty ? dataAnak['Nama'] : '';
        });
        Navigator.of(context).pop();
      } catch (e) {
        Navigator.of(context).pop();
        showSnackBar(context, e.toString());
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: customAppBar(
        context,
        widget.title! + ' ' + widget.level!,
      ),
      body: Column(
        children: [
          SizedBox(height: 20.0),
          Container(
            padding: EdgeInsets.symmetric(
              horizontal: 30.0,
              vertical: 10.0,
            ),
            child: Row(
              children: [
                Expanded(
                  child: TextField(
                    controller: nameController,
                    decoration: kTextField.copyWith(
                      hintText: 'Pilih nama anak',
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(15.0)),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Colors.black,
                        ),
                        borderRadius: BorderRadius.all(Radius.circular(15.0)),
                      ),
                    ),
                    readOnly: true,
                    onTap: pickName,
                  ),
                ),
                SizedBox(width: 20.0),
                CustomButton(
                  color: primaryColor,
                  textColor: Colors.white,
                  text: 'CARI',
                  onPressed: () => onSearch(dataAnak),
                ),
              ],
            ),
          ),
          Expanded(
            child: VisibilityWidget(
              visible:
                  dataAnak.isNotEmpty && dataShalat != null && namaAnak != '',
              replacement: EmptyWidget(),
              child: Column(
                children: [
                  Container(
                    alignment: Alignment.centerLeft,
                    padding: EdgeInsets.symmetric(
                      horizontal: 30.0,
                      vertical: 10.0,
                    ),
                    child: Row(
                      children: [
                        Text('Nama anak : '),
                        Text(
                          namaAnak,
                          style: TextStyle(
                            color: secondaryColor,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 20.0),
                      child: StickyHeadersTable(
                        columnsLength: 6,
                        rowsLength: 30,
                        cellDimensions: CellDimensions.variableColumnWidth(
                          columnWidths: [
                            100.0,
                            100.0,
                            100.0,
                            100.0,
                            100.0,
                            100.0,
                          ],
                          contentCellHeight: 80.0,
                          stickyLegendHeight: 50.0,
                          stickyLegendWidth: 50.0,
                        ),
                        scrollControllers: scrollController,
                        initialScrollOffsetX: _scrollOffsetX,
                        initialScrollOffsetY: _scrollOffsetY,
                        onEndScrolling: (scrollOffsetX, scrollOffsetY) {
                          _scrollOffsetX = scrollOffsetX;
                          _scrollOffsetY = scrollOffsetY;
                        },
                        columnsTitleBuilder: (i) {
                          return Text(listShalat[i]);
                        },
                        rowsTitleBuilder: (i) {
                          return Align(
                            alignment: Alignment.topLeft,
                            child: Container(
                              padding: EdgeInsets.symmetric(
                                vertical: 5.0,
                                horizontal: 8.0,
                              ),
                              width: 35.0,
                              decoration: BoxDecoration(
                                color: primaryColor,
                                borderRadius: BorderRadius.circular(20.0),
                              ),
                              child: Text(
                                (i + 1).toString(),
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          );
                        },
                        contentCellBuilder: (i, j) {
                          final isChecked = dataShalat != null
                              ? dataShalat[(j + 1).toString()][listShalat[i]]
                              : false;

                          return Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15.0),
                              border: Border.all(color: Colors.black),
                              color: isChecked ? secondaryColor : Colors.white,
                            ),
                            margin: EdgeInsets.only(
                              left: 5.0,
                              right: 5.0,
                              bottom: 15.0,
                            ),
                            width: 88.0,
                            height: 70.0,
                          );
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
