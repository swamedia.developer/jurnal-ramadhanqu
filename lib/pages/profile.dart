import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:swamediajurnalramadhanqu/constant/colors.dart';
import 'package:swamediajurnalramadhanqu/constant/db_const.dart';
import 'package:swamediajurnalramadhanqu/constant/images.dart';
import 'package:swamediajurnalramadhanqu/constant/styles.dart';
import 'package:swamediajurnalramadhanqu/helper/custom_button.dart';
import 'package:swamediajurnalramadhanqu/helper/dialogs.dart';
import 'package:swamediajurnalramadhanqu/helper/loading.dart';
import 'package:swamediajurnalramadhanqu/helper/snack_bar.dart';
import 'package:swamediajurnalramadhanqu/provider/login.dart';
import 'package:swamediajurnalramadhanqu/provider/profile.dart';
import 'package:swamediajurnalramadhanqu/widgets/level.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  FirebaseFirestore fireStore = FirebaseFirestore.instance;
  CollectionReference? collection;
  TextEditingController nameController = TextEditingController();
  TextEditingController kelasController = TextEditingController();
  TextEditingController levelController = TextEditingController();
  String? phoneNumber;

  bool isLoading = false;

  @override
  void initState() {
    super.initState();
    getUserProfile();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void getUserProfile() async {
    final prov = Provider.of<ProfileProvider>(context, listen: false);
    final userProfile = prov.userProfile;
    nameController.text = userProfile!['Nama'];
    kelasController.text = userProfile['Kelas'];
    levelController.text = userProfile['Level'];
  }

  Future<void> setProfile(String nama, kelas, level) async {
    final prov = Provider.of<ProfileProvider>(context, listen: false);
    final loginProv = Provider.of<LoginProvider>(context, listen: false);
    phoneNumber = loginProv.firebaseUser!.phoneNumber;
    collection = fireStore.collection(kDbProfile);
    Dialogs.showLoadingDialog(context);
    try {
      await collection!.doc(phoneNumber).update({
        'Nama': nama,
        'Kelas': kelas,
        'Level': level,
      });
    } catch (e) {
      showSnackBar(context, e.toString());
    }
    await prov.getUserProfile(context);
    Navigator.of(context).pop();
    Navigator.of(context).pop(true);
  }

  Future pickLevel() async {
    final result = await showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          content: ListLevel(),
        );
      },
    );
    if (result != null) {
      setState(() {
        levelController.text = result;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    Widget spacer = SizedBox(height: 30.0);
    var screenWidth = MediaQuery.of(context).size.width;
    var screenHeight = MediaQuery.of(context).size.height;

    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(kBackground),
            fit: BoxFit.cover,
          ),
        ),
        child: isLoading
            ? kLoadingWidget(context)
            : ListView(
                children: [
                  Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.only(
                      left: 50.0,
                      right: 50.0,
                      top: screenHeight * 0.1,
                      bottom: 30.0,
                    ),
                    child: Column(
                      children: [
                        Image.asset(
                          kLogoTitle,
                          width: screenWidth * 0.6,
                        ),
                        spacer,
                        Image.asset(
                          kLogoTKSD,
                          width: screenWidth * 0.4,
                        ),
                        spacer,
                        spacer,
                        spacer,
                        TextField(
                          controller: nameController,
                          decoration: kTextField.copyWith(
                            hintText: '',
                            prefixIcon: Container(
                              padding: EdgeInsets.only(
                                left: 15.0,
                                right: 5.0,
                              ),
                              child: Text(
                                'Nama : ',
                                textAlign: TextAlign.center,
                              ),
                            ),
                            prefixIconConstraints: BoxConstraints(
                              minWidth: 0,
                              minHeight: 0,
                            ),
                          ),
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.allow(
                                RegExp(r'[A-Z a-z]')),
                          ],
                        ),
                        SizedBox(height: 10.0),
                        TextField(
                          controller: kelasController,
                          decoration: kTextField.copyWith(
                            hintText: '',
                            prefixIcon: Container(
                              padding: EdgeInsets.only(
                                left: 15.0,
                                right: 5.0,
                              ),
                              child: Text(
                                'Kelas : ',
                                textAlign: TextAlign.center,
                              ),
                            ),
                            prefixIconConstraints: BoxConstraints(
                              minWidth: 0,
                              minHeight: 0,
                            ),
                          ),
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.allow(
                                RegExp(r'[A-Z a-z 0-9]')),
                          ],
                        ),
                        SizedBox(height: 10.0),
                        TextField(
                          controller: levelController,
                          decoration: kTextField.copyWith(
                            hintText: '',
                            prefixIcon: Container(
                              padding: EdgeInsets.only(
                                left: 15.0,
                                right: 5.0,
                              ),
                              child: Text(
                                'Tingkat : ',
                                textAlign: TextAlign.center,
                              ),
                            ),
                            prefixIconConstraints: BoxConstraints(
                              minWidth: 0,
                              minHeight: 0,
                            ),
                          ),
                          readOnly: true,
                          onTap: pickLevel,
                        ),
                        spacer,
                        CustomButton(
                          color: secondaryColor,
                          text: 'Simpan',
                          textColor: Colors.white,
                          onPressed: () {
                            FocusScope.of(context).unfocus();
                            setProfile(
                              nameController.text,
                              kelasController.text,
                              levelController.text,
                            );
                          },
                        ),
                      ],
                    ),
                  ),
                ],
              ),
      ),
    );
  }
}
