import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:swamediajurnalramadhanqu/constant/colors.dart';
import 'package:swamediajurnalramadhanqu/constant/images.dart';
import 'package:swamediajurnalramadhanqu/constant/videos.dart';
import 'package:swamediajurnalramadhanqu/helper/custom_button.dart';
import 'package:swamediajurnalramadhanqu/helper/dialogs.dart';
import 'package:swamediajurnalramadhanqu/provider/login.dart';
import 'package:swamediajurnalramadhanqu/router/constants.dart';
import 'package:video_player/video_player.dart';

class TaujihPage extends StatefulWidget {
  const TaujihPage({Key? key}) : super(key: key);

  @override
  State<TaujihPage> createState() => _TaujihPageState();
}

class _TaujihPageState extends State<TaujihPage> {
  VideoPlayerController? videoPlayer;

  @override
  void initState() {
    super.initState();
    initVideoPlayer();
  }

  @override
  void dispose() {
    super.dispose();
  }

  initVideoPlayer() {
    videoPlayer = VideoPlayerController.asset(kVidUstad)
      ..initialize().then((value) {
        setState(() {});
      });
  }

  void playVideo() async {
    videoPlayer!.play();
    await showDialog(
      context: context,
      builder: (context) {
        return GestureDetector(
          child: Container(
            alignment: Alignment.bottomCenter,
            margin: EdgeInsets.only(
              bottom: MediaQuery.of(context).size.height * 0.1,
            ),
            child: AspectRatio(
              aspectRatio: videoPlayer!.value.aspectRatio,
              child: VideoPlayer(videoPlayer!),
            ),
          ),
          onTap: () {
            if (videoPlayer!.value.isPlaying) {
              videoPlayer!.pause();
            } else {
              videoPlayer!.play();
            }
          },
        );
      },
    );
    videoPlayer!.pause();
    initVideoPlayer();
  }

  Future<void> loginCheck() async {
    final prov = Provider.of<LoginProvider>(context, listen: false);
    Dialogs.showLoadingDialog(context);
    try {
      await prov.isAlreadyAuthenticated();
      Navigator.of(context).pop();
      Navigator.pushNamed(context, mainMenuPage);
    } catch (e) {
      Navigator.of(context).pop();
      Navigator.pushNamed(context, loginPage);
    }
  }

  Future<bool> onWillPop() async {
    Navigator.of(context).pop();
    Navigator.of(context).pop();
    return true;
  }

  @override
  Widget build(BuildContext context) {
    var screenWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      body: WillPopScope(
        onWillPop: onWillPop,
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 30.0),
          color: primaryColor,
          child: Stack(
            alignment: Alignment.topCenter,
            children: [
              Padding(
                padding: EdgeInsets.only(top: 50.0),
                child: Column(
                  children: [
                    Image.asset(
                      kLogoTaujih,
                      width: screenWidth * 0.3,
                    ),
                    SizedBox(height: 20.0),
                    Container(
                      width: screenWidth * 0.35,
                      child: CustomButton(
                        color: secondaryColor,
                        letterSpacing: 5.0,
                        radius: 15.0,
                        text: 'NEXT',
                        textColor: Colors.white,
                        onPressed: () {
                          loginCheck();
                        },
                      ),
                    ),
                  ],
                ),
              ),
              Align(
                alignment: Alignment.bottomLeft,
                child: Image.asset(kImgUstad),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: CustomButton(
                  radius: 50.0,
                  text: 'Klik di sini untuk putar video',
                  color: secondaryColor,
                  textColor: Colors.white,
                  onPressed: () => playVideo(),
                ),
              ),
              // Align(
              //   alignment: Alignment.bottomCenter,
              //   child: InkWell(
              //     child: Container(
              //       margin: EdgeInsets.all(15.0),
              //       padding: EdgeInsets.symmetric(
              //         vertical: 7.0,
              //         horizontal: 30.0,
              //       ),
              //       decoration: BoxDecoration(
              //         borderRadius: BorderRadius.all(Radius.circular(20.0)),
              //         border: Border.all(color: Colors.white),
              //       ),
              //       child: FittedBox(
              //         child: Text(
              //           'Klik di sini untuk putar video',
              //           style: TextStyle(
              //             color: Colors.white,
              //             fontWeight: FontWeight.bold,
              //           ),
              //         ),
              //       ),
              //     ),
              //     onTap: () => playVideo,
              //   ),
              // ),
            ],
          ),
        ),
      ),
    );
  }
}
