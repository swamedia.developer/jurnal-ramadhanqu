import 'package:flutter/material.dart';
import 'package:swamediajurnalramadhanqu/constant/colors.dart';
import 'package:swamediajurnalramadhanqu/constant/images.dart';
import 'package:swamediajurnalramadhanqu/pages/quran.dart';
import 'package:swamediajurnalramadhanqu/widgets/dayMenu.dart';

class DayPage extends StatelessWidget {
  final String? level, title;

  const DayPage({
    Key? key,
    this.level,
    this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var screenHeight = MediaQuery.of(context).size.height;

    Row dayRow(String title, int row) {
      final isFirstRow = row < 2;
      final column1 = isFirstRow ? row : row + (2 * (row - 1));
      final column2 = isFirstRow ? row + 1 : (column1 + 1);
      final column3 = isFirstRow ? row + 2 : (column1 + 2);

      return Row(
        children: [
          DayMenu(
            title: column1.toString(),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (_) => QuranPage(
                    title: title,
                    level: level,
                    day: column1,
                  ),
                ),
              );
            },
          ),
          SizedBox(width: 20.0),
          DayMenu(
            title: column2.toString(),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (_) => QuranPage(
                    title: title,
                    level: level,
                    day: column2,
                  ),
                ),
              );
            },
          ),
          SizedBox(width: 20.0),
          DayMenu(
            title: column3.toString(),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (_) => QuranPage(
                    title: title,
                    level: level,
                    day: column3,
                  ),
                ),
              );
            },
          ),
        ],
      );
    }

    return Scaffold(
      body: Container(
        alignment: Alignment.center,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(kBackground),
            fit: BoxFit.cover,
          ),
        ),
        child: Column(
          children: [
            SizedBox(height: screenHeight * 0.07),
            FittedBox(
              child: Container(
                padding: EdgeInsets.all(20),
                child: FittedBox(
                  child: Text(
                    'Ramadhan hari ke',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: brownColor,
                      fontSize: 28.0,
                    ),
                  ),
                ),
              ),
            ),
            Expanded(
              child: ListView.builder(
                padding: EdgeInsets.all(30.0),
                itemCount: 10,
                itemBuilder: (context, index) {
                  return FittedBox(
                    child: dayRow(title!, index + 1),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
