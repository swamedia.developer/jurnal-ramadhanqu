import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:swamediajurnalramadhanqu/constant/images.dart';
import 'package:swamediajurnalramadhanqu/helper/loading.dart';
import 'package:swamediajurnalramadhanqu/helper/snack_bar.dart';
import 'package:swamediajurnalramadhanqu/pages/day.dart';
import 'package:swamediajurnalramadhanqu/pages/guru/quran.dart';
import 'package:swamediajurnalramadhanqu/pages/guru/shalat.dart';
import 'package:swamediajurnalramadhanqu/pages/guru/shaum.dart';
import 'package:swamediajurnalramadhanqu/pages/shalat.dart';
import 'package:swamediajurnalramadhanqu/pages/shaum.dart';
import 'package:swamediajurnalramadhanqu/provider/profile.dart';
import 'package:swamediajurnalramadhanqu/widgets/buttonMenu.dart';

class SubMenu extends StatefulWidget {
  final String? level;

  const SubMenu({
    Key? key,
    this.level,
  }) : super(key: key);

  @override
  State<SubMenu> createState() => _SubMenuState();
}

class _SubMenuState extends State<SubMenu> {
  String shaum = 'Shaum';
  String murajaah = 'Murajaah';
  String tilawah = 'Tilawah';
  String shalat = 'Shalat';
  bool isGuru = false;
  bool isLoading = false;

  @override
  void initState() {
    super.initState();
    final prov = Provider.of<ProfileProvider>(context, listen: false);
    isGuru = prov.userProfile!['Guru'];
    if (isGuru) {
      getDataAnak();
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future getDataAnak() async {
    isLoading = true;
    final prov = Provider.of<ProfileProvider>(context, listen: false);
    try {
      await prov.getDataAnak(context, widget.level!);
    } catch (e) {
      showSnackBar(context, e.toString());
    }
    setState(() {
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(kBackground),
            fit: BoxFit.cover,
          ),
        ),
        child: isLoading
            ? kLoadAnakWidget(context)
            : Container(
                alignment: Alignment.center,
                padding: EdgeInsets.only(
                  left: 50.0,
                  right: 50.0,
                  top: 50.0,
                ),
                child: FittedBox(
                  child: widget.level == 'SD'
                      ? Column(
                          children: [
                            Row(
                              children: [
                                ButtonMenu(
                                  image: kImgShaum,
                                  title: shaum,
                                  fontSize: 20.0,
                                  onPressed: () {
                                    if (isGuru) {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (_) => GuruShaumPage(
                                            title: shaum,
                                            level: widget.level,
                                          ),
                                        ),
                                      );
                                    } else {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (_) => ShaumPage(
                                            title: shaum,
                                            level: widget.level,
                                          ),
                                        ),
                                      );
                                    }
                                  },
                                ),
                                SizedBox(width: 30.0),
                                ButtonMenu(
                                  image: kImgShalat,
                                  title: shalat,
                                  fontSize: 20.0,
                                  onPressed: () {
                                    if (isGuru) {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (_) => GuruShalatPage(
                                            title: shalat,
                                            level: widget.level,
                                          ),
                                        ),
                                      );
                                    } else {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (_) => ShalatPage(
                                            title: shalat,
                                            level: widget.level,
                                          ),
                                        ),
                                      );
                                    }
                                  },
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                ButtonMenu(
                                  image: kImgQuran,
                                  title: murajaah,
                                  fontSize: 20.0,
                                  onPressed: () {
                                    if (isGuru) {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (_) => GuruQuranPage(
                                            title: murajaah,
                                            level: widget.level,
                                          ),
                                        ),
                                      );
                                    } else {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (_) => DayPage(
                                            title: murajaah,
                                            level: widget.level,
                                          ),
                                        ),
                                      );
                                    }
                                  },
                                ),
                                SizedBox(width: 30.0),
                                ButtonMenu(
                                  image: kImgTilawah,
                                  title: tilawah,
                                  fontSize: 20.0,
                                  onPressed: () {
                                    if (isGuru) {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (_) => GuruQuranPage(
                                            title: tilawah,
                                            level: widget.level,
                                          ),
                                        ),
                                      );
                                    } else {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (_) => DayPage(
                                            title: tilawah,
                                            level: widget.level,
                                          ),
                                        ),
                                      );
                                    }
                                  },
                                ),
                              ],
                            ),
                          ],
                        )
                      : Column(
                          children: [
                            ButtonMenu(
                              image: kImgShaum,
                              title: shaum,
                              fontSize: 20.0,
                              onPressed: () {
                                if (isGuru) {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (_) => GuruShaumPage(
                                        title: shaum,
                                        level: widget.level,
                                      ),
                                    ),
                                  );
                                } else {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (_) => ShaumPage(
                                        title: shaum,
                                        level: widget.level,
                                      ),
                                    ),
                                  );
                                }
                              },
                            ),
                            SizedBox(width: 30.0),
                            ButtonMenu(
                              image: kImgShalat,
                              title: shalat,
                              fontSize: 20.0,
                              onPressed: () {
                                if (isGuru) {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (_) => GuruShalatPage(
                                        title: shalat,
                                        level: widget.level,
                                      ),
                                    ),
                                  );
                                } else {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (_) => ShalatPage(
                                        title: shalat,
                                        level: widget.level,
                                      ),
                                    ),
                                  );
                                }
                              },
                            ),
                            SizedBox(width: 30.0),
                            ButtonMenu(
                              image: kImgQuran,
                              title: murajaah,
                              fontSize: 20.0,
                              onPressed: () {
                                if (isGuru) {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (_) => GuruQuranPage(
                                        title: murajaah,
                                        level: widget.level,
                                      ),
                                    ),
                                  );
                                } else {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (_) => DayPage(
                                        title: murajaah,
                                        level: widget.level,
                                      ),
                                    ),
                                  );
                                }
                              },
                            ),
                          ],
                        ),
                ),
              ),
      ),
    );
  }
}
