import 'package:flutter/material.dart';
import 'package:swamediajurnalramadhanqu/constant/images.dart';

class EmptyWidget extends StatelessWidget {
  final String? description;
  final VoidCallback? action;

  const EmptyWidget({Key? key, this.description, this.action})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Image.asset(
            kImgEmpty,
            height: 120.0,
          ),
          SizedBox(height: 20.0),
          Text(
            description ?? 'Data masih kosong',
            style: theme.textTheme.caption,
          ),
          if (action != null) ...[
            SizedBox(height: 30),
            SizedBox(
              width: 150,
              child: Center(
                child: InkWell(
                  onTap: action,
                  child: Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal: 15,
                      vertical: 8,
                    ),
                    child: Center(
                      child: Text(
                        'Muat ulang',
                        style: theme.textTheme.button!.copyWith(
                          color: Colors.grey,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ],
      ),
    );
  }
}
