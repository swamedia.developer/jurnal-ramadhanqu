import 'package:flutter/material.dart';

class ListLevel extends StatelessWidget {
  const ListLevel({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var screenWidth = MediaQuery.of(context).size.width;

    Widget card(String text) {
      return Card(
        elevation: 10.0,
        child: InkWell(
          child: Container(
            width: screenWidth * 0.5,
            padding: EdgeInsets.symmetric(
              vertical: 15.0,
              horizontal: 20.0,
            ),
            child: Text(
              text,
              textAlign: TextAlign.center,
            ),
          ),
          onTap: () {
            Navigator.of(context).pop(text);
          },
        ),
      );
    }

    return Container(
      padding: EdgeInsets.symmetric(vertical: 20.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            padding: EdgeInsets.only(bottom: 20.0),
            child: Text(
              'Tingkat',
              style: TextStyle(
                fontSize: 22.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          card('TK'),
          card('SD'),
          card('Lainnya'),
        ],
      ),
    );
  }
}
