import 'package:flutter/material.dart';
import 'package:swamediajurnalramadhanqu/constant/colors.dart';

class DayMenu extends StatelessWidget {
  final String? title;
  final VoidCallback? onPressed;

  const DayMenu({
    Key? key,
    this.title,
    this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        GestureDetector(
          child: Card(
            elevation: 10.0,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Container(
              padding: EdgeInsets.symmetric(
                vertical: 20.0,
                horizontal: 20.0,
              ),
              width: 70.0,
              child: Text(
                title!,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: brownColor,
                ),
              ),
            ),
          ),
          onTap: onPressed,
        ),
      ],
    );
  }
}
