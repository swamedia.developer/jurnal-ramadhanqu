import 'package:flutter/material.dart';
import 'package:swamediajurnalramadhanqu/constant/colors.dart';
import 'package:swamediajurnalramadhanqu/constant/images.dart';
import 'package:swamediajurnalramadhanqu/widgets/buttonMenu.dart';

class MenuHeader extends StatelessWidget {
  final String? level, title;

  const MenuHeader({
    Key? key,
    this.level,
    this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(
        left: 50.0,
        right: 50.0,
        top: 70.0,
        bottom: 20.0,
      ),
      decoration: BoxDecoration(
        color: headerColor((level! + title!).toLowerCase()),
        borderRadius: BorderRadius.circular(30.0),
      ),
      child: FittedBox(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ButtonMenu(
              image: level == 'TK' ? kLogoTK : kLogoSD,
              title: level == 'TK' ? 'TK AlQuran' : 'SD AlQuran',
              fontColor: Colors.white,
              fontSize: 32.0,
            ),
            SizedBox(width: 50.0),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 10.0),
                Text(
                  'Kegiatanku Hari ini',
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 25.0,
                  ),
                ),
                SizedBox(height: 30.0),
                Text(
                  'AKU HEBAT',
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 36.0,
                  ),
                ),
                Text(
                  title!.toUpperCase(),
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 58.0,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
