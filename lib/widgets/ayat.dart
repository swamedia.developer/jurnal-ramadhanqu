import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:swamediajurnalramadhanqu/constant/colors.dart';
import 'package:swamediajurnalramadhanqu/constant/styles.dart';
import 'package:swamediajurnalramadhanqu/helper/custom_button.dart';

class Ayat extends StatefulWidget {
  final String? ayat;
  const Ayat({
    Key? key,
    this.ayat,
  }) : super(key: key);

  @override
  State<Ayat> createState() => _AyatState();
}

class _AyatState extends State<Ayat> {
  TextEditingController fromController = TextEditingController();
  TextEditingController toController = TextEditingController();
  String? from = '', to = '';

  @override
  void initState() {
    super.initState();
    if (widget.ayat != '') {
      getFromTo();
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  void getFromTo() {
    from = widget.ayat!.substring(0, widget.ayat!.indexOf(' '));
    to = widget.ayat!.substring(widget.ayat!.lastIndexOf(' ') + 1);
    fromController.text = from!;
    toController.text = to!;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: MediaQuery.of(context).viewInsets,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            padding: EdgeInsets.symmetric(vertical: 20.0),
            child: Text(
              'Ayat',
              style: TextStyle(fontSize: 22.0, fontWeight: FontWeight.bold),
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(
              horizontal: 30.0,
              vertical: 10.0,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  width: 70.0,
                  child: TextField(
                    decoration: kMessageTextField,
                    controller: fromController,
                    textAlign: TextAlign.center,
                    keyboardType: TextInputType.number,
                    inputFormatters: <TextInputFormatter>[
                      FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                    ],
                    onChanged: (value) {
                      setState(() {
                        from = value;
                      });
                    },
                  ),
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 10.0),
                  child: Text('sampai'),
                ),
                Container(
                  width: 70.0,
                  child: TextField(
                    decoration: kMessageTextField,
                    controller: toController,
                    textAlign: TextAlign.center,
                    keyboardType: TextInputType.number,
                    inputFormatters: <TextInputFormatter>[
                      FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                    ],
                    onChanged: (value) {
                      setState(() {
                        to = value;
                      });
                    },
                  ),
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.only(
              top: 10.0,
              bottom: 30.0,
            ),
            child: CustomButton(
              color: from != '' && to != '' ? primaryColor : Colors.grey[400],
              text: 'SIMPAN',
              textColor:
                  from != '' && to != '' ? Colors.white : Colors.grey[500],
              onPressed: from != '' && to != ''
                  ? () {
                      final ayat = from! + ' - ' + to!;
                      Navigator.of(context).pop(ayat);
                    }
                  : null,
            ),
          ),
        ],
      ),
    );
  }
}
