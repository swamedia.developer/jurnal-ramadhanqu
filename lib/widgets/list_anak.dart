import 'package:flutter/material.dart';
import 'package:swamediajurnalramadhanqu/constant/colors.dart';

class ListAnak extends StatelessWidget {
  final List? listAnak;

  const ListAnak({
    Key? key,
    this.listAnak,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 30.0),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.only(bottom: 20.0),
            child: Text(
              'Nama Anak',
              style: TextStyle(fontSize: 22.0, fontWeight: FontWeight.bold),
            ),
          ),
          Expanded(
            child: ListView.builder(
              padding: EdgeInsets.only(
                left: 30.0,
                right: 30.0,
                bottom: 30.0,
              ),
              itemCount: listAnak!.length,
              itemBuilder: (context, index) {
                final nama = listAnak![index]['Nama'];

                return Row(
                  children: [
                    Container(
                      padding: EdgeInsets.symmetric(
                        vertical: 5.0,
                        horizontal: 8.0,
                      ),
                      alignment: Alignment.center,
                      width: 50.0,
                      height: 50.0,
                      decoration: BoxDecoration(
                        color: primaryColor,
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                      child: Text(
                        (index + 1).toString(),
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                    ),
                    SizedBox(width: 10.0),
                    Expanded(
                      child: Card(
                        child: InkWell(
                          child: Container(
                            padding: EdgeInsets.symmetric(
                              vertical: 20.0,
                              horizontal: 20.0,
                            ),
                            child: Text(
                              nama,
                              style: TextStyle(
                                fontSize: 18.0,
                              ),
                            ),
                          ),
                          onTap: () {
                            Navigator.of(context).pop(listAnak![index]);
                          },
                        ),
                      ),
                    ),
                  ],
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
