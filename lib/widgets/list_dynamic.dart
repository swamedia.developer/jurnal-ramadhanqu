import 'package:flutter/material.dart';

class ListDynamic extends StatelessWidget {
  final dynamic data;
  final String? title;
  final bool isCenterText;

  const ListDynamic({
    Key? key,
    this.data,
    this.title,
    this.isCenterText = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 30.0),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.only(bottom: 20.0),
            child: Text(
              title!,
              style: TextStyle(fontSize: 22.0, fontWeight: FontWeight.bold),
            ),
          ),
          Expanded(
            child: ListView.builder(
              padding: EdgeInsets.only(
                left: 30.0,
                right: 30.0,
                bottom: 30.0,
              ),
              itemCount: data.length,
              itemBuilder: (context, index) {
                String content = data[index].toString();

                return Row(
                  children: [
                    Expanded(
                      child: Card(
                        child: InkWell(
                          child: Container(
                            padding: EdgeInsets.symmetric(
                              vertical: 20.0,
                              horizontal: 20.0,
                            ),
                            child: Text(
                              content,
                              textAlign: isCenterText
                                  ? TextAlign.center
                                  : TextAlign.left,
                              style: TextStyle(
                                fontSize: 18.0,
                              ),
                            ),
                          ),
                          onTap: () {
                            Navigator.of(context).pop(content);
                          },
                        ),
                      ),
                    ),
                  ],
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
