import 'package:flutter/material.dart';
import 'package:swamediajurnalramadhanqu/constant/colors.dart';

class CheckBoxShaum extends StatelessWidget {
  final int? number;
  final bool isChecked, isGuru;
  final VoidCallback? onTap;

  const CheckBoxShaum({
    Key? key,
    this.number,
    this.isChecked = false,
    this.isGuru = false,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        GestureDetector(
          onTap: onTap,
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(30.0),
              border: Border.all(color: Colors.black),
              color: !isGuru
                  ? Colors.white
                  : isChecked
                      ? secondaryColor
                      : Colors.white,
            ),
            width: 88.0,
            height: 88.0,
            child: !isGuru
                ? isChecked
                    ? Icon(Icons.check)
                    : null
                : Container(),
          ),
        ),
        Align(
          alignment: Alignment.topLeft,
          child: Container(
            padding: EdgeInsets.symmetric(
              vertical: 5.0,
              horizontal: 8.0,
            ),
            width: 35.0,
            decoration: BoxDecoration(
              color: primaryColor,
              borderRadius: BorderRadius.circular(20.0),
            ),
            child: Text(
              number.toString(),
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.white,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
