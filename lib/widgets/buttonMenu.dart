import 'package:flutter/material.dart';
import 'package:swamediajurnalramadhanqu/constant/colors.dart';

class ButtonMenu extends StatelessWidget {
  final String? image, title;
  final VoidCallback? onPressed;
  final double? fontSize;
  final Color? fontColor;

  const ButtonMenu({
    Key? key,
    this.image,
    this.title,
    this.fontSize = 16.0,
    this.onPressed,
    this.fontColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        GestureDetector(
          child: Card(
            elevation: 10.0,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(30.0),
            ),
            child: Container(
              padding: EdgeInsets.all(10.0),
              child: Image.asset(image!),
            ),
          ),
          onTap: onPressed,
        ),
        Container(
          padding: EdgeInsets.only(
            top: 20.0,
            bottom: 30.0,
          ),
          child: FittedBox(
            child: Text(
              title!,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: fontSize,
                color: fontColor != null ? fontColor : brownColor,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
