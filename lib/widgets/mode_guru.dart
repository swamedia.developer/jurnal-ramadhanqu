import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:swamediajurnalramadhanqu/constant/colors.dart';
import 'package:swamediajurnalramadhanqu/constant/styles.dart';
import 'package:swamediajurnalramadhanqu/helper/custom_button.dart';
import 'package:swamediajurnalramadhanqu/provider/profile.dart';

class ModeGuru extends StatefulWidget {
  const ModeGuru({Key? key}) : super(key: key);

  @override
  State<ModeGuru> createState() => _ModeGuruState();
}

class _ModeGuruState extends State<ModeGuru> {
  TextEditingController codeController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void onExit() {
    showDialog(
      context: context, 
      builder: (context) {
        return AlertDialog(
          title: Text('Peringatan'),
          content: Text('Yakin mau keluar dari Mode Guru?'),
          actions: [
            Padding(
              padding: EdgeInsets.only(bottom: 10.0),
              child: CustomButton(
                color: redColor,
                textColor: Colors.white,
                text: 'Tidak',
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 10.0),
              child: CustomButton(
                color: primaryColor,
                textColor: Colors.white,
                text: 'Ya',
                onPressed: () {
                  Navigator.of(context).pop();
                  Navigator.of(context).pop('exit');
                },
              ),
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final prov = Provider.of<ProfileProvider>(context, listen: false);
    final isGuru = prov.userProfile!['Guru'];

    return Container(
      padding: MediaQuery.of(context).viewInsets,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            padding: EdgeInsets.only(
              top: 30.0,
              bottom: 20.0,
            ),
            child: Text(
              isGuru ? 'Anda berada di Mode Guru' : 'Konfirmasi Mode Guru',
              style: TextStyle(
                fontSize: 22.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          isGuru
              ? Container()
              : Container(
                  padding: EdgeInsets.symmetric(horizontal: 30.0),
                  child: TextField(
                    controller: codeController,
                    decoration: kTextField.copyWith(
                      hintText: '',
                      prefixIcon: Container(
                        padding: EdgeInsets.only(
                          left: 15.0,
                          right: 5.0,
                        ),
                        child: Text(
                          'Kode : ',
                          textAlign: TextAlign.center,
                        ),
                      ),
                      prefixIconConstraints: BoxConstraints(
                        minWidth: 0,
                        minHeight: 0,
                      ),
                    ),
                    inputFormatters: <TextInputFormatter>[
                      FilteringTextInputFormatter.allow(
                          RegExp(r'[A-Z a-z, 0-9]')),
                    ],
                  ),
                ),
          Container(
            padding: EdgeInsets.only(
              top: 20.0,
              bottom: 50.0,
            ),
            child: CustomButton(
              color: primaryColor,
              text: isGuru ? 'Keluar dari Mode Guru' : 'Verifikasi Kode',
              textColor: Colors.white,
              onPressed: () {
                if (isGuru) {
                  onExit();
                }
                else {
                  Navigator.of(context).pop(codeController.text);
                }
              },
            ),
          ),
        ],
      ),
    );
  }
}
